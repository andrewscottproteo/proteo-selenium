﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium.Interactions;
using System.Collections.ObjectModel;

namespace SeleniumTests
{
    [TestFixture]
    public class AddSubcontractor
    {
        private IWebDriver driver;
        private StringBuilder verificationErrors;
        private bool acceptNextAlert = true;
        private List<string> urlList;
        private String orderIdInvoice;
        private String batchId;

        [SetUp]
        public void SetupTest()
        {
            verificationErrors = new StringBuilder();

            urlList = new List<string>();
            //urlList.Add(CommonLogic.testChilterns);
            //urlList.Add(CommonLogic.demo);
            //urlList.Add(CommonLogic.testFagan);
            urlList.Add(CommonLogic.testFirmin);
            //urlList.Add(CommonLogic.testJackRichards);
            //urlList.Add(CommonLogic.testKersey);
            //urlList.Add(CommonLogic.testKnowles);
            //urlList.Add(CommonLogic.testNicholls);
            //urlList.Add(CommonLogic.testWilliams);
            //urlList.Add(CommonLogic.testWoodall);
        }


        [TearDown]
        public void TeardownTest()
        {
            try
            {
                driver.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
            Assert.AreEqual("", verificationErrors.ToString());
        }

        [Test]
        public void AddOrderTest()
        {
            foreach (var s in urlList)
            {
                try
                {
                    theTest(s);
                }
                catch (Exception ex)
                {

                    if (CommonLogic.addedDriver == true)
                    {
                        driver.SwitchTo().Window(CommonLogic.mainWindow);

                        CommonLogic.deleteDriver(CommonLogic.displayName, driver);
                    }

                    if (CommonLogic.addedVehicle == true)
                    {
                        driver.Navigate().Refresh();

                        CommonLogic.deleteVehicle(CommonLogic.vehicleName, driver);
                    }

                    if (CommonLogic.addedTrailer == true)
                    {
                        driver.Navigate().Refresh();

                        CommonLogic.deleteTrailer(CommonLogic.trailerRef, driver);
                    }

                    if (CommonLogic.addedControlArea == true)
                    {
                        driver.SwitchTo().Window(CommonLogic.mainWindow);

                        CommonLogic.unassignFromControlArea(CommonLogic.clientName, driver);
                    }

                    else
                    {
                        throw new Exception(s + " failed!,  " + ex.Message.ToString());
                    }
                }
            }
        }


        private void theTest(string url)
        {
            driver = new ChromeDriver("C:\\webdrivers");

            CommonLogic.login(url, driver);

            CommonLogic.addClient(url, driver);

            CommonLogic.storeControlAreas(url, driver);

            CommonLogic.assignToControlArea(url, driver);

            CommonLogic.addTrailer(url, driver);

            CommonLogic.addVehicle(url, driver);

            CommonLogic.addDriver(url, driver);

            CommonLogic.addSubcontractorUser(url, driver);

            CommonLogic.addSubcontractor(url, driver);

            CommonLogic.addOrder(url, driver);

            // There are three different ways to subcontract, so two must be commented out at any one time

            CommonLogic.completeSubcontractedRunWholeJob(url, driver);

            //CommonLogic.completeSubcontractedRunSpecificLegs(url, driver);

            //CommonLogic.completeSubcontractedRunPerOrder(url, driver);

            CommonLogic.flagSubcontractedRun(url, driver);

            CommonLogic.multiBatchSubcontracted(url, driver);

            deleteOrder(url);

            CommonLogic.deleteSubcontractor(url, driver);

            CommonLogic.deleteUser(url, driver);

        }


        private void deleteOrder(string url)

        {

            driver.SwitchTo().Window(CommonLogic.mainWindow);

            if (!driver.FindElement(By.Id("ctl00_cblSearchType_1")).Selected)
            {
                driver.FindElement(By.Id("ctl00_cblSearchType_1")).Click();
            }

            driver.FindElement(By.Id("ctl00_txtSearchString")).Clear();
            driver.FindElement(By.Id("ctl00_txtSearchString")).SendKeys(CommonLogic.runIdInvoice);
            driver.FindElement(By.Id("ctl00_txtSearchString")).SendKeys(Keys.Enter);

            Thread.Sleep(10000);

            CommonLogic.runWindow = driver.WindowHandles.Last();

            driver.SwitchTo().Window(CommonLogic.runWindow);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ctl20_repOH_ctl01_btnRecordCallIn")).Click();

            Thread.Sleep(5000);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ctl20_repOH_ctl02_btnRecordCallIn")).Click();

            Thread.Sleep(5000);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_lnkCancelJobAndOrders")).Click();

            Thread.Sleep(2000);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ButtonOk")).Click();

            Thread.Sleep(10000);

            driver.SwitchTo().Window(CommonLogic.mainWindow);

            CommonLogic.unassignFromControlArea(CommonLogic.clientName, driver);

            Thread.Sleep(5000);

            CommonLogic.deleteDriver(CommonLogic.displayName, driver);

            Thread.Sleep(5000);

            CommonLogic.deleteVehicle(CommonLogic.vehicleName, driver);

            Thread.Sleep(5000);

            CommonLogic.deleteTrailer(CommonLogic.trailerRef, driver);

        }

        private bool IsElementPresent(By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        private bool IsAlertPresent()
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }

        private string CloseAlertAndGetItsText()
        {
            try
            {
                IAlert alert = driver.SwitchTo().Alert();
                string alertText = alert.Text;
                if (acceptNextAlert)
                {
                    alert.Accept();
                }
                else
                {
                    alert.Dismiss();
                }
                return alertText;
            }
            finally
            {
                acceptNextAlert = true;
            }
        }
    }
}