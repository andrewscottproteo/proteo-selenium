﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Interactions;
using System.Collections.ObjectModel;

namespace SeleniumTests
{
    public static class CommonLogic
    {
        public static string mainWindow = "";
        public static string runWindow = "";
        public static string orderWindow = "";
        public static string resourceWindow = "";
        public static string preInvoiceWindow = "";
        public static string trafficSheetWindow = "";
        public static string resourceUnitsWindow = "";
        public static string legPlanningWindow = "";

        public static List<string> SelectedTrafficArea = new List<string>();
        public static string ta = SelectedTrafficArea.FirstOrDefault();

        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static string RandomLetters(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static string RandomLowerCaseLetters(int length)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyz";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());

        }

        public static string RandomNumber(int length)
        {
            const string chars = "0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static void TodaysDate(string url, IWebDriver driver)
        {
            var todaysDate = DateTime.Now;
            var tomorrowsDate = todaysDate.AddDays(1);
            var twoDaysAhead = todaysDate.AddDays(2);
            var threeDaysAhead = todaysDate.AddDays(3);
        }

        // Applicable to multiple screens
        public static string displayName = "displayname" + CommonLogic.RandomLowerCaseLetters(12);
        public static string firstName = "firstname" + CommonLogic.RandomString(5);
        public static string lastName = "lastname" + CommonLogic.RandomString(10);
        public static string fullName = firstName + " " + lastName;
        public static string mobilePhone = 07 + CommonLogic.RandomNumber(9);

        public static string todaysDate = DateTime.Now.ToString("dd/MM/yyyy");


        // Create Driver
        public static string driverPasscode = CommonLogic.RandomNumber(4);
        public static string plannerName = firstName + " " + lastName;
        public static bool addedDriver = false;

        // Create Vehicle
        public static string vehicleName = "vehiclename" + CommonLogic.RandomString(12);
        public static string vehicleRegNo = CommonLogic.RandomString(7);
        public static bool addedVehicle = false;

        // Create Trailer
        public static string trailerRef = "trailerref" + CommonLogic.RandomString(12);
        public static bool addedTrailer = false;

        // Create Client or Organisation
        public static string clientName = "clientName" + CommonLogic.RandomString(5);
        public static string postalCode = CommonLogic.RandomString(8);
        public static string address = CommonLogic.RandomString(8);
        public static string postalTown = CommonLogic.RandomString(8);
        public static string county = CommonLogic.RandomString(8);

        // Create Subcontractor

        public static string subcontractorName = "subcontractorName" + CommonLogic.RandomString(5);
        public static string subcontractorEmail = "j.richardson@proteo.co.uk";

        // Create User
        public static string username = "username" + CommonLogic.RandomLowerCaseLetters(5);
        public static string emailAddress = CommonLogic.RandomLowerCaseLetters(5) + "@" + CommonLogic.RandomLowerCaseLetters(5) + ".com";
        public static string password = "Testtest12345!";
        public static string newPassword = "Testtest123456!";
        public static string planner = CommonLogic.RandomLetters(2);

        // Add New Point
        public static string pointNotes = "newPoint" + CommonLogic.RandomString(20);

        // Add New Reference
        public static string referenceName = "reference" + CommonLogic.RandomNumber(4);

        // Add New Tarrif
        public static string tarrifDescription = "tarrifDescription" + CommonLogic.RandomString(10);
        public static string tableDescription = "tableDescription" + CommonLogic.RandomString(5);

        // Add New Extra
        public static string extraShortDescription = "shortDescription" + CommonLogic.RandomString(3);
        public static string extraDescription = "Description" + CommonLogic.RandomString(8);

        // Client Username
        public static string clientUsername = "clientUser" + CommonLogic.RandomString(5);

        public static string defaultProteo = "proteo";

        // Control Areas
        public static bool addedControlArea = false;

        // Client 
        public static bool addedClient = false;

        // Traffic Sheet
        public static string selectedControlArea = "";
        public static string selectedTrafficArea1 = "";
        public static string selectedTrafficArea2 = "";
        public static string selectedResourceDepot = "";

        // Adding Order

        public static string palletNumbers = "1";
        public static string rate = "1.0";

        // URLS to test
        public static string demo = ("demo.proteoenterprise.co.uk");
        public static string testChilterns = ("test.chiltern.proteoenterprise.co.uk");
        public static string testFagan = ("test.fandw.proteoenterprise.co.uk");
        public static string testFirmin = ("test.firmin.proteoenterprise.co.uk");
        public static string testJackRichards = ("test.jr.proteoenterprise.co.uk");
        public static string testKersey = ("test.kersey.proteoenterprise.co.uk");
        public static string testKnowles = ("test.knowles.proteoenterprise.co.uk");
        public static string testNicholls = ("test.nicholls.proteoenterprise.co.uk");
        public static string testWilliams = ("test.williams.proteoenterprise.co.uk");
        public static string testWoodall = ("test.woodall.proteoenterprise.co.uk");

        private static List<string> urlList;
        public static String clientNameInvoice;
        public static String collectInvoice;
        public static String deliverInvoice;
        public static String deliverInvoiceSubContract;
        public static String orderIdInvoice;
        public static String runIdInvoice;
        public static String batchId;
        public static String customerName;

        private static StringBuilder verificationErrors;

        public static void SetupTest()
        {
            verificationErrors = new StringBuilder();
        }

        public static void login(string url, IWebDriver driver)
        {
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("https://" + url + "/security/login.aspx?ReturnUrl=%2f");

            //Thread.Sleep(10000);

            //    IWebElement runLink = driver.FindElements(dr(By.LinkText("Run"));

            //    // Sometimes the first Business Type will not trigger the Run checkbox, this tries the second one

            //        if (!runLink.Displayed)
            //        {
            //        driver.Navigate().GoToUrl("https://jenkins.datainsights.ae/login?from=%2F");
            //        driver.FindElement(By.Name("j_username")).Clear();
            //        driver.FindElement(By.Name("j_username")).SendKeys("jamesr");
            //        driver.FindElement(By.Name("j_password")).Clear();
            //        driver.FindElement(By.Name("j_password")).SendKeys("JAdH@^&#gg");
            //        driver.FindElement(By.Id("yui-gen1-button")).Click();
            //        Thread.Sleep(3000);

            //        if (url == demo)
            //        {
            //            driver.FindElement(By.XPath("//tr[@id='job_Demo Proteo Enterprise - ON']/td[7]/a")).Click();
            //        }

            //        else if (url == testChilterns)
            //        {
            //            driver.FindElement(By.XPath("//tr[@id='job_PE-TEST-1 - ON']/td[7]/a")).Click();
            //        }

            //        else if (url == testFagan)
            //        {
            //            driver.FindElement(By.XPath("//tr[@id='job_Fagan and Whalley Proteo Enterprise TEST - ON']/td[7]/a")).Click();
            //        }

            //        else if (url == testJackRichards)
            //        {
            //            driver.FindElement(By.XPath("//tr[@id='job_JackRichards Proteo Enterprise TEST - ON']/td[7]/a")).Click();
            //        }

            //        else if (url == testKersey)
            //        {
            //            driver.FindElement(By.XPath("//tr[@id='job_PE-TEST-1 - ON']/td[7]/a")).Click();
            //        }

            //        else if (url == testKnowles)
            //        {
            //            driver.FindElement(By.XPath("//tr[@id='job_Knowles Proteo Enterprise TEST - ON']/td[7]/a")).Click();
            //        }

            //        else if (url == testNicholls)
            //        {
            //            driver.FindElement(By.XPath("//tr[@id='job_Nicholls Proteo Enterprise TEST - ON']/td[7]/a")).Click();
            //        }

            //        else if (url == testWilliams)
            //        {
            //            driver.FindElement(By.XPath("//tr[@id='job_PE-TEST-1 - ON']/td[7]/a")).Click();
            //        }

            //        else if (url == testWoodall)
            //        {
            //            driver.FindElement(By.XPath("//tr[@id='job_PE-TEST-1 - ON']/td[7]/a")).Click();
            //        }

            //        Thread.Sleep(15000);

            //        driver.Navigate().GoToUrl("https://" + url + "/security/login.aspx?ReturnUrl=%2f");
            //    }

            driver.FindElement(By.Id("txtUserName")).Clear();
            driver.FindElement(By.Id("txtUserName")).SendKeys("sa");
            driver.FindElement(By.Id("txtPIN")).Clear();
            driver.FindElement(By.Id("txtPIN")).SendKeys("J3r3myb!");
            driver.FindElement(By.Id("btnLogon")).Click();

            mainWindow = driver.CurrentWindowHandle;

            driver.SwitchTo().Window(mainWindow);
        }

        public static void logOff(string url, IWebDriver driver)
        {
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Help'])[1]/following::span[1]")).Click();
            driver.Close();
        }


        public static void addOrderNoRun(string url, IWebDriver driver)
        {
            var timeout = 10000; // in milliseconds
            var wait = new WebDriverWait(driver, TimeSpan.FromMilliseconds(timeout));

            mainWindow = driver.WindowHandles.Last();

            driver.SwitchTo().Window(mainWindow);

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.PartialLinkText("Orders"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.PartialLinkText("Add Order"))).Click();

            var currentWindow = driver.CurrentWindowHandle;
            IList<string> allWindowHandles = driver.WindowHandles;
            if (allWindowHandles.Count > 1)

            {
                CommonLogic.orderWindow = driver.WindowHandles.Last();

                driver.SwitchTo().Window(CommonLogic.orderWindow);
            }

            // Add order screen

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_ucOrder_cboClient_Input")));
            IWebElement clientInput = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_cboClient_Input"));

            if (clientName == null)
            {
                clientInput.Clear();
                clientInput.SendKeys(defaultProteo);
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//li[@class='rcbHovered']"))).Click();
            }

            else
            {
                clientInput.Clear();
                clientInput.SendKeys(clientName);
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//li[@class='rcbHovered']"))).Click();
            }

            Thread.Sleep(3000);

            new SelectElement(driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_cboBusinessType"))).SelectByIndex(2);

            IWebElement selectCollectFrom = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_ucCollectionPoint_cboPoint_Input"));
            selectCollectFrom.SendKeys(defaultProteo);
            Thread.Sleep(3000);
            selectCollectFrom.SendKeys(Keys.Down);
            selectCollectFrom.SendKeys(Keys.Enter);

            Thread.Sleep(2000);

            IWebElement selectDeliverTo = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_ucDeliveryPoint_cboPoint_Input"));
            selectDeliverTo.Click();
            Thread.Sleep(3000);
            selectDeliverTo.SendKeys(Keys.Down);
            selectDeliverTo.SendKeys(Keys.Enter);

            Thread.Sleep(4000);

            try
            {
                IWebElement enterNoOfPallets = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_rntxtPallets"));
                enterNoOfPallets.Clear();
                enterNoOfPallets.SendKeys(palletNumbers);

                IWebElement enterCases = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_rntxtCartons"));
                enterCases.Clear();
                enterCases.SendKeys(palletNumbers);

            }
            catch (Exception et)
            {

                var t = et.ToString();
                IWebElement enterFull = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_rntxtPalletFull"));
                enterFull.Clear();
                enterFull.SendKeys(palletNumbers);

                IWebElement enterHalf = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_rntxtPalletHalf"));
                enterHalf.Clear();
                enterHalf.SendKeys(palletNumbers);

                IWebElement enterQtr = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_rntxtPalletQtr"));
                enterQtr.Clear();
                enterQtr.SendKeys(palletNumbers);

                IWebElement enterOver = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_rntxtPalletOver"));
                enterOver.Clear();
                enterOver.SendKeys(palletNumbers);

                IWebElement enterSpaces = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_rntxtPalletSpaces"));
                enterSpaces.Clear();
                enterSpaces.SendKeys(palletNumbers);
            }

            IWebElement enterWeight = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_rntxtWeight"));
            enterWeight.Clear();
            enterWeight.SendKeys(palletNumbers);

            IWebElement enterRate = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_rntOrderRate"));
            enterRate.Clear();
            enterRate.SendKeys(palletNumbers);

            // These three variables are for referring back to these elements on the invoicing stages

            clientNameInvoice = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_cboClient_Input")).GetAttribute("value");

            collectInvoice = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_dteCollectionFromDate_dateInput")).GetAttribute("value");

            deliverInvoice = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_dteDeliveryByDate_dateInput")).GetAttribute("value");

            IWebElement addOrderButton = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_btnSubmit"));

            addOrderButton.Click();
            Thread.Sleep(4000);
        }
        public static void addOrder(string url, IWebDriver driver)
        {
            var timeout = 10000; // in milliseconds
            var wait = new WebDriverWait(driver, TimeSpan.FromMilliseconds(timeout));

            mainWindow = driver.WindowHandles.Last();

            driver.SwitchTo().Window(mainWindow);

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.PartialLinkText("Orders"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.PartialLinkText("Add Order"))).Click();

            var currentWindow = driver.CurrentWindowHandle;
            IList<string> allWindowHandles = driver.WindowHandles;
            if (allWindowHandles.Count > 1)

            {
                CommonLogic.orderWindow = driver.WindowHandles.Last();

                driver.SwitchTo().Window(CommonLogic.orderWindow);
            }

            // Add order screen

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_ucOrder_cboClient_Input")));
            IWebElement clientInput = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_cboClient_Input"));

            if (clientName == null)
            {
                clientInput.Clear();
                clientInput.SendKeys(defaultProteo);
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//li[@class='rcbHovered']"))).Click();
            }

            else
            {
                clientInput.Clear();
                clientInput.SendKeys(clientName);
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//li[@class='rcbHovered']"))).Click();
            }

            Thread.Sleep(3000);

            new SelectElement(driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_cboBusinessType"))).SelectByIndex(1);

            IWebElement runCheckbox = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_chkCreateJob"));

            // Sometimes the first Business Type will not trigger the Run checkbox, this tries the second one

            if (!runCheckbox.Displayed)
            {
                new SelectElement(driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_cboBusinessType"))).SelectByIndex(2);
                runCheckbox.Click();
            }
            else

            {
                if (!runCheckbox.Selected)
                {
                    runCheckbox.Click();
                }
            }

            IWebElement selectCollectFrom = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_ucCollectionPoint_cboPoint_Input"));
            selectCollectFrom.SendKeys(defaultProteo);
            Thread.Sleep(3000);
            selectCollectFrom.SendKeys(Keys.Down);
            selectCollectFrom.SendKeys(Keys.Enter);

            Thread.Sleep(2000);

            IWebElement selectDeliverTo = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_ucDeliveryPoint_cboPoint_Input"));
            selectDeliverTo.Click();
            Thread.Sleep(3000);
            selectDeliverTo.SendKeys(Keys.Down);
            selectDeliverTo.SendKeys(Keys.Enter);

            Thread.Sleep(4000);

            try
            {
                IWebElement enterNoOfPallets = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_rntxtPallets"));
                enterNoOfPallets.Clear();
                enterNoOfPallets.SendKeys(palletNumbers);

                IWebElement enterCases = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_rntxtCartons"));
                enterCases.Clear();
                enterCases.SendKeys(palletNumbers);

            }
            catch (Exception et)
            {

                var t = et.ToString();
                IWebElement enterFull = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_rntxtPalletFull"));
                enterFull.Clear();
                enterFull.SendKeys(palletNumbers);

                IWebElement enterHalf = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_rntxtPalletHalf"));
                enterHalf.Clear();
                enterHalf.SendKeys(palletNumbers);

                IWebElement enterQtr = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_rntxtPalletQtr"));
                enterQtr.Clear();
                enterQtr.SendKeys(palletNumbers);

                IWebElement enterOver = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_rntxtPalletOver"));
                enterOver.Clear();
                enterOver.SendKeys(palletNumbers);

                IWebElement enterSpaces = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_rntxtPalletSpaces"));
                enterSpaces.Clear();
                enterSpaces.SendKeys(palletNumbers);
            }

            IWebElement enterWeight = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_rntxtWeight"));
            enterWeight.Clear();
            enterWeight.SendKeys(palletNumbers);

            IWebElement enterRate = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_rntOrderRate"));
            enterRate.Clear();
            enterRate.SendKeys(rate);

            // These three variables are for referring back to these elements on the invoicing stages

            clientNameInvoice = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_cboClient_Input")).GetAttribute("value");

            collectInvoice = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_dteCollectionFromDate_dateInput")).GetAttribute("value");

            deliverInvoice = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_dteDeliveryByDate_dateInput")).GetAttribute("value");

            var dt = Convert.ToDateTime(deliverInvoice);
            dt = dt.AddDays(1);

            deliverInvoiceSubContract = dt.ToString("dd/MM/yy");

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_btnSubmit")).Click();
            Thread.Sleep(4000);
        }


        public static void completeRun(string url, IWebDriver driver)
        {
            var timeout = 10000; // in milliseconds
            var wait = new WebDriverWait(driver, TimeSpan.FromMilliseconds(timeout));

            driver.SwitchTo().Window(driver.WindowHandles.Last());
            runWindow = driver.WindowHandles.Last();

            var currentWindow = driver.CurrentWindowHandle;
            IList<string> allWindowHandles = driver.WindowHandles;
            if (allWindowHandles.Count > 2)

            {
                driver.SwitchTo().Window(orderWindow);
                driver.SwitchTo().Frame(0);
                orderIdInvoice = driver.FindElement(By.Id("lblOrderID")).Text;
                driver.Close();
                orderWindow = "";
            }

            else
            {
                driver.SwitchTo().Window(mainWindow);
                driver.SwitchTo().Frame(0);
                orderIdInvoice = driver.FindElement(By.Id("lblOrderID")).Text;
                orderWindow = "";
                //TODO - find a way to click this close button
                // this does not work
                //driver.FindElement(By.XPath("//button[contains(text(),'Close Window')]")).Click();
            }

            driver.SwitchTo().Window(runWindow);

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_grdTrafficSheet_ctl00__0"))).Click();
            IWebElement resourceThis = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_grdTrafficSheet_ctl00__0"));
            Actions rightClick = new Actions(driver);
            rightClick.ContextClick(resourceThis).Perform();
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//span[contains(text(), 'Resource This')]"))).Click();

            Thread.Sleep(2000);

            // Resource This Screen

            driver.SwitchTo().Window(driver.WindowHandles.Last());
            resourceWindow = driver.WindowHandles.Last();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_cboDriver_Input")));
            IWebElement enterDriver = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboDriver_Input"));
            enterDriver.SendKeys(displayName);
            Thread.Sleep(2000);
            enterDriver.SendKeys(Keys.Down);
            driver.FindElement(By.ClassName("rcbHovered")).Click();

            IWebElement enterVehicle = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboVehicle_Input"));
            if (enterVehicle != null)
            {
                enterVehicle.GetAttribute("value");
            }
            else
            {
                enterVehicle.SendKeys(displayName);
                Thread.Sleep(2000);
                enterVehicle.SendKeys(Keys.Down);
                driver.FindElement(By.ClassName("rcbHovered")).Click();
            }

            IWebElement enterTrailer = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboTrailer_Input"));
            enterTrailer.SendKeys(displayName);
            Thread.Sleep(2000);
            enterTrailer.SendKeys(Keys.Down);
            driver.FindElement(By.ClassName("rcbHovered")).Click();

            IWebElement updateResourceButton = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_btnUpdateAndConfirm"));
            updateResourceButton.Click();
            Thread.Sleep(4000);

            driver.SwitchTo().Window(runWindow);

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_grdTrafficSheet_ctl00__0")));
            IWebElement quickCommunicateThis = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_grdTrafficSheet_ctl00__0"));
            Actions rightClick2 = new Actions(driver);
            rightClick2.ContextClick(quickCommunicateThis).Perform();
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//span[contains(text(), 'Quick Communicate This')]"))).Click();

            Thread.Sleep(5000);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ctl20_repOH_ctl01_btnRecordCallIn")).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_btnAddMoveNext"))).Click();

            Thread.Sleep(5000);

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_btnAddMoveNext"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_lblJobId")));
            runIdInvoice = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_lblJobId")).Text;

            driver.Close();

            runWindow = "";

            driver.SwitchTo().Window(mainWindow);

            Thread.Sleep(4000);
        }

        public static void addClient(string url, IWebDriver driver)
        {
            var timeout = 10000; // in milliseconds
            var wait = new WebDriverWait(driver, TimeSpan.FromMilliseconds(timeout));

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Clients/Client Customers"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("List Clients"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Only Show Subcontractors Missing SubContractor T&Cs'])[1]/following::input[2]"))).Click();

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtOrganisationName")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtOrganisationName")).SendKeys(clientName);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtOrganisationDisplayName")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtOrganisationDisplayName")).SendKeys(displayName);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtPostCode")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtPostCode")).SendKeys(postalCode);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtAddressLine1")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtAddressLine1")).SendKeys(address);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtAddressLine2")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtAddressLine2")).SendKeys(address);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtAddressLine3")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtAddressLine3")).SendKeys(address);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtPostTown")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtPostTown")).SendKeys(postalTown);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtCounty")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtCounty")).SendKeys(county);

            if (driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_chkSuspended")).Selected)
            {
                driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_chkSuspended")).Click();
            }

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_btnAdd")).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_lblConfirmation")));

            try
            {
                Assert.AreEqual("The Organisation has been successfully added.", driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_lblConfirmation")).Text);
            }
            catch (AssertionException e)
            {
                verificationErrors.Append(e.Message);
            }
            Thread.Sleep(5000);

            addedClient = true;
        }

        public static void deleteClient(string url, IWebDriver driver)
        {
            var timeout = 10000; // in milliseconds
            var wait = new WebDriverWait(driver, TimeSpan.FromMilliseconds(timeout));

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Clients/Client Customers"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("List Clients"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_grdOrganisations_ctl00_ctl02_ctl00_txtSearch"))).Clear();

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_grdOrganisations_ctl00_ctl02_ctl00_txtSearch")).SendKeys(CommonLogic.clientName);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_grdOrganisations_ctl00_ctl02_ctl00_txtSearch")).SendKeys(Keys.Enter);

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_grdOrganisations_ctl00_ctl02_ctl00_txtSearch")));
            driver.FindElement(By.LinkText(CommonLogic.clientName)).Click();

            if (!driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_chkSuspended")).Selected)
            {
                driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_chkSuspended")).Click();
            }

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_btnAdd")).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='View Fleet'])[1]/following::span[1]"))).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Add New Point'])[1]/following::span[2]")).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("filterOptionsDiv"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_optShowSuspended"))).Click();

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_grdOrganisations_ctl00_ctl02_ctl00_txtSearch")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_grdOrganisations_ctl00_ctl02_ctl00_txtSearch")).SendKeys(CommonLogic.clientName);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_grdOrganisations_ctl00_ctl02_ctl00_txtSearch")).SendKeys(Keys.Enter);
            Thread.Sleep(5000);

            Assert.IsTrue(!driver.FindElement(By.LinkText(CommonLogic.clientName)).Selected);
        }


        public static void storeControlAreas(string trailerRef, IWebDriver driver)
        {
            var timeout = 10000; // in milliseconds
            var wait = new WebDriverWait(driver, TimeSpan.FromMilliseconds(timeout));

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Run"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Traffic Sheet"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Current Window"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//h1[contains(text(), 'Traffic Sheet')]")));

            driver.SwitchTo().Frame(1);

            driver.FindElement(By.XPath("//table[@id='filters']/tbody/tr/td[3]/img[1]")).Click();

            trafficSheetWindow = driver.WindowHandles.Last();

            driver.SwitchTo().Window(trafficSheetWindow);

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("cboControlArea")));

            if (driver.FindElement(By.Id("cboControlArea")).Displayed)
            {
                List<IWebElement> controlAreaTable = driver.FindElements(By.XPath("//table[@id='cboControlArea']/tbody/tr/td")).ToList();
                List<string> SelectedControlArea = new List<string>();
                foreach (var row in controlAreaTable)
                {
                    if (row.Text.Trim() != "")
                    {
                        // find element and test if checked
                        var label = driver.FindElements(By.XPath("//table[@id='cboControlArea']/tbody/tr/td/label[contains(text(), '" + row.Text.Trim() + "')]/parent::td/input[@checked]"));
                        if (label.Count > 0)
                        {
                            SelectedControlArea.Add(row.Text);
                        }
                    }
                }

                // you now have a list of selected items.

                Console.WriteLine(SelectedControlArea);

                CommonLogic.selectedControlArea = SelectedControlArea.FirstOrDefault();
            }


            // Finding the selected Traffic Areas

            if (driver.FindElement(By.Id("cboTrafficAreas")).Displayed)
            {
                List<IWebElement> trafficAreasTable = driver.FindElements(By.XPath("//table[@id='cboTrafficAreas']/tbody/tr/td")).ToList();

                SelectedTrafficArea = new List<string>();
                foreach (var row in trafficAreasTable)
                {
                    if (row.Text.Trim() != "")
                    {
                        // find element and test if checked
                        var label = driver.FindElements(By.XPath("//table[@id='cboTrafficAreas']/tbody/tr/td/label[contains(text(), '" + row.Text.Trim() + "')]/parent::td/input[@checked]"));
                        if (label.Count > 0)
                        {
                            SelectedTrafficArea.Add(row.Text);
                        }
                    }
                }

                // Adding list of items into Strings

                foreach (var ta in SelectedTrafficArea)
                {
                    ta.ToString();
                }

                selectedTrafficArea1 = SelectedTrafficArea.FirstOrDefault();

                if (SelectedTrafficArea.Count > 1)
                {
                    selectedTrafficArea2 = SelectedTrafficArea.ElementAt(1);
                }
            }

            // Resource depot

            SelectElement selectDepot = new SelectElement(driver.FindElement(By.Id("cboDepot")));
            IWebElement depotSelection = selectDepot.SelectedOption;

            // Added if statement because if the Depot is set to the default text then we don't want to use this
            if (depotSelection.Text == "Use Control Area and Traffic Areas to determine resource pool")
            {
                Console.WriteLine("Default text used for the depot, not using");
            }
            else
            {
                selectedResourceDepot = depotSelection.Text;
            }
            driver.Close();

            driver.SwitchTo().Window(driver.WindowHandles.Last());

        }

        public static void assignToControlArea(string url, IWebDriver driver)
        {
            var timeout = 10000; // in milliseconds
            var wait = new WebDriverWait(driver, TimeSpan.FromMilliseconds(timeout));

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Administration"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Control Areas"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//h1[contains(text(), 'Control Area Management')]")));

            // click the Control Area based on the control area selected

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//*[text()[contains(.,'" + selectedControlArea + "')]]"))).Click();

            driver.FindElement(By.CssSelector("[ng-click='addCustomer()']")).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.CssSelector("[ng-model='customer.selectedCustomer']")));

            IWebElement addCustomer = driver.FindElement(By.CssSelector("[ng-model='customer.selectedCustomer']"));
            addCustomer.Clear();
            addCustomer.SendKeys(clientName);
            addCustomer.SendKeys(Keys.Enter);

            Thread.Sleep(5000);

            driver.FindElement(By.Id("btnAddCustomer")).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//h1[contains(text(), 'Control Area Management')]")));

            addedControlArea = true;
        }

        public static void unassignFromControlArea(string url, IWebDriver driver)
        {
            var timeout = 10000; // in milliseconds
            var wait = new WebDriverWait(driver, TimeSpan.FromMilliseconds(timeout));

            driver.SwitchTo().Window(mainWindow);

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Administration"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Control Areas"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//h1[contains(text(), 'Control Area Management')]")));

            Thread.Sleep(1000);

            driver.FindElement(By.XPath("//*[text()[contains(.,'" + CommonLogic.selectedControlArea + "')]]")).Click();

            Thread.Sleep(1000);

            driver.FindElement(By.XPath("//*[text()[contains(.,'" + clientName + "')]]")).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.CssSelector("[ng-really-click='removeCustomer()']"))).Click();

            Thread.Sleep(1000);

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.CssSelector("[ng-click='ok()']"))).Click();

        }

        public static void addDriver(string url, IWebDriver driver)
        {
            var timeout = 20000; // in milliseconds
            var wait = new WebDriverWait(driver, TimeSpan.FromMilliseconds(timeout));

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Resources"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Drivers"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("View All"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Send Message'])[1]/following::button[1]"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.CssSelector("[ng-model='driver.individual.titleId']")));
            SelectElement titleDropdown = new SelectElement(driver.FindElement(By.CssSelector("[ng-model='driver.individual.titleId']")));
            titleDropdown.SelectByIndex(1);

            driver.FindElement(By.Id("firstnames")).Clear();
            driver.FindElement(By.Id("firstnames")).SendKeys(firstName);
            driver.FindElement(By.Id("lastname")).Clear();
            driver.FindElement(By.Id("lastname")).SendKeys(lastName);
            driver.FindElement(By.Id("displayName")).Clear();
            driver.FindElement(By.Id("displayName")).SendKeys(displayName);
            driver.FindElement(By.Id("passcode")).Clear();
            driver.FindElement(By.Id("passcode")).SendKeys(driverPasscode);
            SelectElement driverType = new SelectElement(driver.FindElement(By.Name("drivertype")));
            driverType.SelectByIndex(1);
            driver.FindElement(By.Id("posttown")).Clear();
            driver.FindElement(By.Id("posttown")).SendKeys(CommonLogic.postalTown);

            SelectElement usualVehicle = new SelectElement(driver.FindElement(By.XPath("//select[@ng-model='driver.assignedVehicleResourceID']")));
            if (vehicleName == "")
            {
                usualVehicle.SelectByIndex(1);

            }
            else
            {
                usualVehicle.SelectByText(vehicleName);
            }

            SelectElement driversDepot = new SelectElement(driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)=concat('Driver', \"'\", 's Depot')])[1]/following::select[1]")));

            if (selectedResourceDepot != "")
            {
                driversDepot.SelectByText(selectedResourceDepot);
            }

            else
            {
                driversDepot.SelectByIndex(1);
            }

            SelectElement driverControlArea = new SelectElement(driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='This driver is a'])[1]/following::select[1]")));

            if (selectedControlArea != null)
            {
                driverControlArea.SelectByText(selectedControlArea);
            }

            else
            {
                driverControlArea.SelectByIndex(1);
            }

            SelectElement driverTrafficArea = new SelectElement(driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='This driver is a'])[1]/following::select[2]")));

            if (selectedTrafficArea1 != null)
            {
                driverTrafficArea.SelectByText(selectedTrafficArea1);
            }

            else
            {
                driverTrafficArea.SelectByIndex(1);
            }

            SelectElement planner = new SelectElement(driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Planner'])[1]/following::select[1]")));
            planner.SelectByIndex(1);

            driver.FindElement(By.Id("mobilephone")).Clear();
            driver.FindElement(By.Id("mobilephone")).SendKeys(CommonLogic.mobilePhone);

            Thread.Sleep(500);

            driver.FindElement(By.Id("workemail")).Clear();
            driver.FindElement(By.Id("workemail")).SendKeys(CommonLogic.emailAddress);

            SelectElement prefCommunication = new SelectElement(driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Pref. Communication'])[1]/following::select[1]")));
            prefCommunication.SelectByIndex(1);

            IWebElement enterOrganisation = driver.FindElement(By.CssSelector("[ng-model='driver.point.organizationName']"));
            enterOrganisation.SendKeys(CommonLogic.defaultProteo);
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//li[@class='uib-typeahead-match ng-scope active']"))).Click();

            IWebElement enterPoint = driver.FindElement(By.CssSelector("[ng-model='driver.point.companyName']"));
            enterPoint.SendKeys(CommonLogic.defaultProteo);
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//li[@class='uib-typeahead-match ng-scope active']"))).Click();

            Thread.Sleep(2000);

            driver.FindElement(By.CssSelector("[ng-click='saveDriver()']")).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.CssSelector("[ng-model='tableParams.filter().$']")));
            driver.FindElement(By.CssSelector("[ng-model='tableParams.filter().$']")).Clear();
            driver.FindElement(By.CssSelector("[ng-model='tableParams.filter().$']")).SendKeys(displayName);

            Thread.Sleep(2000);

            Assert.IsTrue(driver.FindElement(By.XPath("//a[contains(text(),'" + displayName + "')]")).Displayed);

            addedDriver = true;
            Thread.Sleep(4000);
        }

        public static void deleteDriver(string displayName, IWebDriver driver)
        {
            var timeout = 10000; // in milliseconds
            var wait = new WebDriverWait(driver, TimeSpan.FromMilliseconds(timeout));

            try
            {
                driver.Navigate().Refresh();

                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Resources"))).Click();

                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Drivers"))).Click();

                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("View All"))).Click();

                driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Remote Assistance'])[1]/following::input[2]")).Clear();
                driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Remote Assistance'])[1]/following::input[2]")).SendKeys(displayName);

                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText(displayName)));

                driver.FindElement(By.LinkText(displayName)).Click();

                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Driver Deleted'])[1]/following::input[1]"))).Click();

                driver.FindElement(By.CssSelector("[ng-click='saveDriver()']")).Click();

                displayName = "";
                Thread.Sleep(4000);
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void addVehicle(string url, IWebDriver driver)
        {
            var timeout = 20000; // in milliseconds
            var wait = new WebDriverWait(driver, TimeSpan.FromMilliseconds(timeout));

            try
            {

                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Resources"))).Click();

                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Vehicles"))).Click();

                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("View All"))).Click();

                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Remote Assistance'])[1]/following::button[2]"))).Click();

                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.CssSelector("[ng-model='vehicle.VehicleName']"))).Clear();
                driver.FindElement(By.CssSelector("[ng-model='vehicle.VehicleName']")).SendKeys(vehicleName);

                driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Registration No'])[1]/following::input[1]")).Clear();
                driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Registration No'])[1]/following::input[1]")).SendKeys(vehicleRegNo);

                SelectElement vehicleManufacturer = new SelectElement(driver.FindElement(By.Name("manufacturer")));
                vehicleManufacturer.SelectByIndex(1);

                Thread.Sleep(500);

                SelectElement vehicleModel = new SelectElement(driver.FindElement(By.Name("model")));
                vehicleModel.SelectByIndex(1);

                Thread.Sleep(500);

                SelectElement vehicleClass = new SelectElement(driver.FindElement(By.Name("class")));
                vehicleClass.SelectByIndex(1);

                driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Next Service Date'])[1]/following::button[2]")).Click();

                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("servicedate")));

                driver.FindElement(By.Id("servicedate")).Clear();
                driver.FindElement(By.Id("servicedate")).SendKeys(todaysDate);
                driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[1]/following::button[1]")).Click();
                driver.FindElement(By.ClassName("close")).Click();

                Thread.Sleep(500);

                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='MOT Expires On'])[1]/following::button[1]")));

                driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='MOT Expires On'])[1]/following::button[1]")).Click();
                driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Sat'])[1]/following::span[11]")).Click();

                driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Next Service Date'])[1]/following::button[1]")).Click();
                driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Sat'])[1]/following::button[18]")).Click();


                SelectElement vehicleType = new SelectElement(driver.FindElement(By.Name("vehicleType")));
                vehicleType.SelectByIndex(1);

                SelectElement vehicleNominal = new SelectElement(driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Nominal Code'])[1]/following::select[1]")));
                vehicleNominal.SelectByIndex(1);

                SelectElement vehicleDepot = new SelectElement(driver.FindElement(By.Name("vehicledepot")));

                if (selectedResourceDepot != "")
                {
                    vehicleDepot.SelectByText(selectedResourceDepot);
                }

                else
                {
                    vehicleDepot.SelectByIndex(1);
                }

                SelectElement vehicleControlArea = new SelectElement(driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='This vehicle is a'])[1]/following::select[1]")));

                if (selectedControlArea != null)
                {
                    vehicleControlArea.SelectByText(selectedControlArea);
                }

                else
                {
                    vehicleControlArea.SelectByIndex(1);
                }

                SelectElement vehicleTrafficArea = new SelectElement(driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='This vehicle is a'])[1]/following::select[2]")));

                if (selectedTrafficArea1 != null)
                {
                    vehicleTrafficArea.SelectByText(selectedTrafficArea1);
                }

                else
                {
                    vehicleTrafficArea.SelectByIndex(1);
                }

                driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Dedicated to client'])[1]/following::input[1]")).Clear();
                driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Dedicated to client'])[1]/following::input[1]")).SendKeys(defaultProteo);
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//li[@class='uib-typeahead-match ng-scope active']"))).Click();

                IWebElement enterOrganisation = driver.FindElement(By.CssSelector("[ng-model='vehicle.point.organizationName']"));
                enterOrganisation.SendKeys(defaultProteo);
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//li[@class='uib-typeahead-match ng-scope active']"))).Click();

                IWebElement enterPoint = driver.FindElement(By.CssSelector("[ng-model='vehicle.point.companyName']"));
                enterPoint.SendKeys(defaultProteo);
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//li[@class='uib-typeahead-match ng-scope active']"))).Click();

                driver.FindElement(By.CssSelector("[ng-click='saveVehicle()']")).Click();

                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.CssSelector("[ng-model='tableParams.filter().$']")));
                driver.FindElement(By.CssSelector("[ng-model='tableParams.filter().$']")).Clear();
                driver.FindElement(By.CssSelector("[ng-model='tableParams.filter().$']")).SendKeys(vehicleName);

                Thread.Sleep(2000);

                Assert.IsTrue(driver.FindElement(By.XPath("//a[contains(text(),'"+ vehicleName +"')]")).Displayed);          

                addedVehicle = true;
                Thread.Sleep(4000);
            }

            catch (Exception ex)
            {

                throw ex;
            }
        }


        public static void deleteVehicle(string vehicleName, IWebDriver driver)
        {
            var timeout = 10000; // in milliseconds
            var wait = new WebDriverWait(driver, TimeSpan.FromMilliseconds(timeout));

            try
            {
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Resources"))).Click();

                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Vehicles"))).Click();

                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("View All"))).Click();

                driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Remote Assistance'])[1]/following::input[2]")).Clear();
                driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Remote Assistance'])[1]/following::input[2]")).SendKeys(vehicleName);

                Thread.Sleep(3000);

                if (driver.FindElement(By.LinkText(vehicleName)) != null)
                {
                    driver.FindElement(By.LinkText(vehicleName)).Click();
                    wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Is Deleted'])[1]/following::input[1]"))).Click();
                    driver.FindElement(By.CssSelector("[ng-click='saveVehicle()']")).Click();

                    vehicleName = "";
                    Thread.Sleep(4000);
                }
                else
                {
                    throw new Exception("Vehicle was not found!!");
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void addTrailer(string url, IWebDriver driver)
        {
            var timeout = 20000; // in milliseconds
            var wait = new WebDriverWait(driver, TimeSpan.FromMilliseconds(timeout));

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Resources"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Trailers"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("View All"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_grdTrailers_ctl00_ctl02_ctl00_btnAddNewTrailer"))).Click();

            driver.SwitchTo().Window(driver.WindowHandles.Last());

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtTrailerRef")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtTrailerRef")).SendKeys(trailerRef);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtDisplayName")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtDisplayName")).SendKeys(displayName);

            SelectElement trailerManufacturer = new SelectElement(driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboTrailerManufacturer")));
            trailerManufacturer.SelectByIndex(1);

            SelectElement trailerType = new SelectElement(driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboTrailerType")));
            trailerType.SelectByIndex(1);

            SelectElement trailerDescription = new SelectElement(driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboTrailerDescription")));
            trailerDescription.SelectByIndex(1);

            IWebElement enterOrganisation = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboOrganisation_Input"));
            enterOrganisation.Clear();
            enterOrganisation.SendKeys(defaultProteo);
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//li[@class='rcbHovered']"))).Click();

            Thread.Sleep(1000);

            IWebElement enterPoint = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboPoint_Input"));
            enterPoint.Clear();
            enterPoint.SendKeys(defaultProteo);
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//li[@class='rcbHovered']"))).Click();
            
            Thread.Sleep(1000);

            SelectElement trailerControlArea = new SelectElement(driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboControlArea")));

            if (selectedControlArea != null)
            {
                trailerControlArea.SelectByText(selectedControlArea);
            }

            else
            {
                trailerControlArea.SelectByIndex(1);
            }


            SelectElement trailerTrafficArea = new SelectElement(driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboTrafficArea")));

            if (selectedTrafficArea1 != null)
            {
                trailerTrafficArea.SelectByText(selectedTrafficArea1);
            }

            else
            {
                trailerTrafficArea.SelectByIndex(1);
            }

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_btnAdd")).Click();

            driver.SwitchTo().Window(driver.WindowHandles.Last());

            driver.FindElement(By.Id("qs777585")).Clear();
            driver.FindElement(By.Id("qs777585")).SendKeys(displayName);

            Thread.Sleep(2000);

            Assert.IsTrue(driver.FindElement(By.XPath("//td[contains(text(),'"+ displayName + "')]")).Displayed);

            addedTrailer = true;
        }

        public static void deleteTrailer(string url, IWebDriver driver)
        {
            var timeout = 10000; // in milliseconds
            var wait = new WebDriverWait(driver, TimeSpan.FromMilliseconds(timeout));

            try
            {
                driver.Navigate().Refresh();

                driver.SwitchTo().Window(driver.WindowHandles.FirstOrDefault());

                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Resources"))).Click();

                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Trailers"))).Click();

                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("View All"))).Click();

                driver.FindElement(By.ClassName("qs_input")).Clear();
                driver.FindElement(By.ClassName("qs_input")).SendKeys(trailerRef);

                if (driver.FindElement(By.LinkText(trailerRef)) != null)
                {
                    driver.FindElement(By.LinkText(trailerRef)).Click();

                    driver.SwitchTo().Window(driver.WindowHandles.Last());

                    wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_btnAdd"))).Click();

                    trailerRef = "";
                }
                else
                {
                    throw new Exception("Trailer was not found!!");
                }

            }
            catch (Exception ex)
            {
                //throw ex;
            }
        }

        public static void addSubcontractor(string url, IWebDriver driver)
        {
            var timeout = 10000; // in milliseconds
            var wait = new WebDriverWait(driver, TimeSpan.FromMilliseconds(timeout));

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Sub-Contractors"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Add New Sub-Contractor"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_txtOrganisationName"))).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtOrganisationName")).SendKeys(CommonLogic.subcontractorName);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtOrganisationDisplayName")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtOrganisationDisplayName")).SendKeys(CommonLogic.displayName);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtPostCode")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtPostCode")).SendKeys(CommonLogic.postalCode);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtAddressLine1")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtAddressLine1")).SendKeys(CommonLogic.address);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtAddressLine2")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtAddressLine2")).SendKeys(CommonLogic.address);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtAddressLine3")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtAddressLine3")).SendKeys(CommonLogic.address);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtPostTown")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtPostTown")).SendKeys(CommonLogic.address);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtCounty")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtCounty")).SendKeys(CommonLogic.address);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_btnAdd")).Click();
        }

        public static void completeSubcontractedRunWholeJob(string url, IWebDriver driver)
        {
            var timeout = 10000; // in milliseconds
            var wait = new WebDriverWait(driver, TimeSpan.FromMilliseconds(timeout));

            driver.SwitchTo().Window(driver.WindowHandles.Last());
            runWindow = driver.WindowHandles.Last();

            var currentWindow = driver.CurrentWindowHandle;
            IList<string> allWindowHandles = driver.WindowHandles;
            if (allWindowHandles.Count > 2)

            {
                driver.SwitchTo().Window(orderWindow);
                driver.SwitchTo().Frame(0);
                orderIdInvoice = driver.FindElement(By.Id("lblOrderID")).Text;
                driver.Close();
                orderWindow = "";
            }

            else
            {
                driver.SwitchTo().Window(mainWindow);
                driver.SwitchTo().Frame(0);
                orderIdInvoice = driver.FindElement(By.Id("lblOrderID")).Text;
            }

            driver.SwitchTo().Window(runWindow);

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_grdTrafficSheet_ctl00__0"))).Click();
            IWebElement resourceThis = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_grdTrafficSheet_ctl00__0"));
            Actions rightClick = new Actions(driver);
            rightClick.ContextClick(resourceThis).Perform();
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//span[contains(text(), 'Sub-Contract Leg')]"))).Click();

            Thread.Sleep(2000);

            // Sub-Contract Leg

            driver.SwitchTo().Window(driver.WindowHandles.Last());
            resourceWindow = driver.WindowHandles.Last();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_cboSubContractor_Input")));
            IWebElement enterSubcontractor = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboSubContractor_Input"));
            enterSubcontractor.SendKeys(subcontractorName);
            Thread.Sleep(2000);
            enterSubcontractor.SendKeys(Keys.Enter);

            IWebElement enterSubcontractorEmail = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtSendJCConfirmationEmails"));
            enterSubcontractorEmail.Clear();
            enterSubcontractorEmail.SendKeys(subcontractorEmail);

            IWebElement enterSubcontractorRate = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_rntSubContractRate"));
            enterSubcontractorRate.Clear();
            enterSubcontractorRate.SendKeys(rate);

            IWebElement enterSubcontractorContactNumber = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboFContactNumber_Input"));
            enterSubcontractorContactNumber.Clear();
            enterSubcontractorContactNumber.SendKeys(mobilePhone);

            IWebElement enterSubcontractorSpecialInstructions = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtSpecialInstructions"));
            enterSubcontractorSpecialInstructions.Clear();
            enterSubcontractorSpecialInstructions.SendKeys(defaultProteo);

            // Selecting whole job
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_rdoSubContractMethod_0")).Click();

            Thread.Sleep(5000);

            Assert.IsTrue(driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_rdoSubContractMethod_0")).Selected);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_btnConfirmSubContract")).Click();

            Thread.Sleep(5000);

            driver.SwitchTo().Window(runWindow);

            // The subcontractor has been succesfully added so the name should be visible here
            Assert.IsTrue(driver.FindElement(By.XPath("//td[contains(text(), '"+ subcontractorName +"')]")).Displayed);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ctl20_repOH_ctl01_btnRecordCallIn")).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_btnAddMoveNext"))).Click();

            Thread.Sleep(5000);

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_btnAddMoveNext"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_lblJobId")));
            runIdInvoice = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_lblJobId")).Text;

            driver.Close();

            runWindow = "";

            driver.SwitchTo().Window(mainWindow);

            Thread.Sleep(4000);
        }

        public static void completeSubcontractedRunSpecificLegs(string url, IWebDriver driver)
        {
            var timeout = 10000; // in milliseconds
            var wait = new WebDriverWait(driver, TimeSpan.FromMilliseconds(timeout));

            driver.SwitchTo().Window(driver.WindowHandles.Last());
            runWindow = driver.WindowHandles.Last();

            var currentWindow = driver.CurrentWindowHandle;
            IList<string> allWindowHandles = driver.WindowHandles;
            if (allWindowHandles.Count > 2)

            {
                driver.SwitchTo().Window(orderWindow);
                driver.SwitchTo().Frame(0);
                orderIdInvoice = driver.FindElement(By.Id("lblOrderID")).Text;
                driver.Close();
                orderWindow = "";
            }

            else
            {
                driver.SwitchTo().Window(mainWindow);
                driver.SwitchTo().Frame(0);
                orderIdInvoice = driver.FindElement(By.Id("lblOrderID")).Text;
            }

            driver.SwitchTo().Window(runWindow);

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_grdTrafficSheet_ctl00__0"))).Click();
            IWebElement resourceThis = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_grdTrafficSheet_ctl00__0"));
            Actions rightClick = new Actions(driver);
            rightClick.ContextClick(resourceThis).Perform();
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//span[contains(text(), 'Sub-Contract Leg')]"))).Click();

            Thread.Sleep(2000);

            // Sub-Contract Leg

            driver.SwitchTo().Window(driver.WindowHandles.Last());
            resourceWindow = driver.WindowHandles.Last();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_cboSubContractor_Input")));
            IWebElement enterSubcontractor = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboSubContractor_Input"));
            enterSubcontractor.SendKeys(subcontractorName);
            Thread.Sleep(2000);
            enterSubcontractor.SendKeys(Keys.Enter);

            IWebElement enterSubcontractorEmail = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtSendJCConfirmationEmails"));
            enterSubcontractorEmail.Clear();
            enterSubcontractorEmail.SendKeys(subcontractorEmail);

            IWebElement enterSubcontractorRate = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_rntSubContractRate"));
            enterSubcontractorRate.Clear();
            enterSubcontractorRate.SendKeys(rate);

            IWebElement enterSubcontractorContactNumber = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboFContactNumber_Input"));
            enterSubcontractorContactNumber.Clear();
            enterSubcontractorContactNumber.SendKeys(mobilePhone);

            IWebElement enterSubcontractorSpecialInstructions = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtSpecialInstructions"));
            enterSubcontractorSpecialInstructions.Clear();
            enterSubcontractorSpecialInstructions.SendKeys(defaultProteo);

            // Selecting Specific Legs
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_rdoSubContractMethod_1")).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_grdLegs_ctl00_ctl02_ctl00_checkboxSelectColumnSelectCheckBoxt"))).Click();

            Thread.Sleep(5000);

            Assert.IsTrue(driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_rdoSubContractMethod_0")).Selected);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_btnConfirmSubContract")).Click();

            Thread.Sleep(5000);

            driver.SwitchTo().Window(runWindow);

            // The subcontractor has been succesfully added so the name should be visible here
            Assert.IsTrue(driver.FindElement(By.XPath("//td[contains(text(), '" + subcontractorName + "')]")).Displayed);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ctl20_repOH_ctl01_btnRecordCallIn")).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_btnAddMoveNext"))).Click();

            Thread.Sleep(5000);

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_btnAddMoveNext"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_lblJobId")));
            runIdInvoice = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_lblJobId")).Text;

            driver.Close();

            runWindow = "";

            driver.SwitchTo().Window(mainWindow);

            Thread.Sleep(4000);
        }

        public static void completeSubcontractedRunPerOrder(string url, IWebDriver driver)
        {
            var timeout = 10000; // in milliseconds
            var wait = new WebDriverWait(driver, TimeSpan.FromMilliseconds(timeout));

            driver.SwitchTo().Window(driver.WindowHandles.Last());
            runWindow = driver.WindowHandles.Last();

            var currentWindow = driver.CurrentWindowHandle;
            IList<string> allWindowHandles = driver.WindowHandles;
            if (allWindowHandles.Count > 2)

            {
                driver.SwitchTo().Window(orderWindow);
                driver.SwitchTo().Frame(0);
                orderIdInvoice = driver.FindElement(By.Id("lblOrderID")).Text;
                driver.Close();
                orderWindow = "";
            }

            else
            {
                driver.SwitchTo().Window(mainWindow);
                driver.SwitchTo().Frame(0);
                orderIdInvoice = driver.FindElement(By.Id("lblOrderID")).Text;
            }

            driver.SwitchTo().Window(runWindow);

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_grdTrafficSheet_ctl00__0"))).Click();
            IWebElement resourceThis = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_grdTrafficSheet_ctl00__0"));
            Actions rightClick = new Actions(driver);
            rightClick.ContextClick(resourceThis).Perform();
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//span[contains(text(), 'Sub-Contract Leg')]"))).Click();

            Thread.Sleep(2000);

            // Sub-Contract Leg

            driver.SwitchTo().Window(driver.WindowHandles.Last());
            resourceWindow = driver.WindowHandles.Last();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_cboSubContractor_Input")));
            IWebElement enterSubcontractor = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboSubContractor_Input"));
            enterSubcontractor.SendKeys(subcontractorName);
            Thread.Sleep(2000);
            enterSubcontractor.SendKeys(Keys.Enter);

            IWebElement enterSubcontractorEmail = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtSendJCConfirmationEmails"));
            enterSubcontractorEmail.Clear();
            enterSubcontractorEmail.SendKeys(subcontractorEmail);

            IWebElement enterSubcontractorRate = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_rntSubContractRate"));
            enterSubcontractorRate.Clear();
            enterSubcontractorRate.SendKeys(rate);

            IWebElement enterSubcontractorContactNumber = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboFContactNumber_Input"));
            enterSubcontractorContactNumber.Clear();
            enterSubcontractorContactNumber.SendKeys(mobilePhone);

            IWebElement enterSubcontractorSpecialInstructions = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtSpecialInstructions"));
            enterSubcontractorSpecialInstructions.Clear();
            enterSubcontractorSpecialInstructions.SendKeys(defaultProteo);

            // Selecting Per Order
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_rdoSubContractMethod_2")).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_grdLegs_ctl00_ctl02_ctl00_checkboxSelectColumnSelectCheckBox"))).Click();

            Thread.Sleep(5000);

            Assert.IsTrue(driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_rdoSubContractMethod_0")).Selected);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_btnConfirmSubContract")).Click();

            Thread.Sleep(5000);

            driver.SwitchTo().Window(runWindow);

            // The subcontractor has been succesfully added so the name should be visible here
            Assert.IsTrue(driver.FindElement(By.XPath("//td[contains(text(), '" + subcontractorName + "')]")).Displayed);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ctl20_repOH_ctl01_btnRecordCallIn")).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_btnAddMoveNext"))).Click();

            Thread.Sleep(5000);

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_btnAddMoveNext"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_lblJobId")));
            runIdInvoice = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_lblJobId")).Text;

            driver.Close();

            runWindow = "";

            driver.SwitchTo().Window(mainWindow);

            Thread.Sleep(4000);
        }

        public static void flagSubcontractedRun(string url, IWebDriver driver)
        {
            var timeout = 20000; // in milliseconds
            var wait = new WebDriverWait(driver, TimeSpan.FromMilliseconds(timeout));

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Invoicing"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Sub-Contractor"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Auto Batch Run"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_rdiStartDate_dateInput")));

            Thread.Sleep(3000);
            IWebElement subcontractorDateFrom = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_rdiStartDate_dateInput"));
            subcontractorDateFrom.Clear();
            subcontractorDateFrom.SendKeys(collectInvoice);
            
            IWebElement subcontractorDateTo = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_rdiEndDate_dateInput"));
            subcontractorDateTo.Clear();     
            subcontractorDateTo.SendKeys(deliverInvoiceSubContract);

            IWebElement subcontractor = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboSubContractor_Input"));
            subcontractor.Clear();
            subcontractor.SendKeys(subcontractorName);
            Thread.Sleep(2000);
            subcontractor.SendKeys(Keys.Enter);

            if (!driver.FindElement(By.Id("chkBusinessTypeAll")).Selected)
              {
                driver.FindElement(By.Id("chkBusinessTypeAll")).Click();
              }

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_btnRefresh")).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText(runIdInvoice)));

            IWebElement subcontractorInvoiceDate = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_rdiInvoiceDate_dateInput"));
            subcontractorInvoiceDate.Clear();
            subcontractorInvoiceDate.SendKeys(todaysDate);

            driver.FindElement(By.XPath("//a[contains(text(),'" + runIdInvoice + "')]/parent::td/parent::tr/td[1]/input[@type='checkbox']")).Click();

            Assert.IsTrue(driver.FindElement(By.XPath("//a[contains(text(),'" + runIdInvoice + "')]/parent::td/parent::tr/td[1]/input[@type='checkbox']")).Selected);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_btnCreateBatch")).Click();

            Thread.Sleep(5000);

            driver.FindElement(By.XPath("//td[contains(text(),'" + fullName + "')]/parent::tr/td[1]/input[@type='checkbox']")).Click();

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_btnCreateBatch")).Click();

            Thread.Sleep(4000);
        }

        public static void addSubcontractorUser(string url, IWebDriver driver)
        {
            var timeout = 10000; // in milliseconds
            var wait = new WebDriverWait(driver, TimeSpan.FromMilliseconds(timeout));

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Administration"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Users"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("List Users"))).Click();

            Thread.Sleep(2000);

            driver.FindElement(By.CssSelector("[ng-click='addUser()']")).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Name("username"))).Clear();
            driver.FindElement(By.Name("username")).SendKeys(CommonLogic.username);
            driver.FindElement(By.Name("firstname")).Clear();
            driver.FindElement(By.Name("firstname")).SendKeys(CommonLogic.firstName);
            driver.FindElement(By.Name("lastname")).Clear();
            driver.FindElement(By.Name("lastname")).SendKeys(CommonLogic.lastName);
            driver.FindElement(By.Name("password")).Clear();
            driver.FindElement(By.Name("password")).SendKeys(CommonLogic.password);
            driver.FindElement(By.Name("confirm")).Clear();
            driver.FindElement(By.Name("confirm")).SendKeys(CommonLogic.password);
            driver.FindElement(By.Name("email")).Clear();
            driver.FindElement(By.Name("email")).SendKeys(CommonLogic.emailAddress);
            SelectElement selectSecurityRoles = new SelectElement(driver.FindElement(By.XPath(".//*[@id='bootstrap-duallistbox-nonselected-list_']")));
            selectSecurityRoles.SelectByText("Invoicing");
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='show all'])[1]/following::button[2]")).Click();
            Thread.Sleep(2000);
            driver.FindElement(By.Id("btnSave")).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.CssSelector("[ng-model='searchUser']"))).Clear();
            driver.FindElement(By.CssSelector("[ng-model='searchUser']")).SendKeys(username);
            Thread.Sleep(5000);
            Assert.IsTrue(driver.FindElement(By.XPath("//td[contains(text(),'" + emailAddress + "')]")).Displayed);
        }


        public static void flagOrder(string url, IWebDriver driver)
        {
            var timeout = 10000; // in milliseconds
            var wait = new WebDriverWait(driver, TimeSpan.FromMilliseconds(timeout));

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Invoicing"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Client Invoicing"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Flag Orders as Ready to Invoice"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("filterOptionsDiv"))).Click();

            // Entering the variables from before to locate the Order just created
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_rdiStartDate_dateInput")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_rdiStartDate_dateInput")).SendKeys(CommonLogic.collectInvoice);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_rdiEndDate_dateInput")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_rdiEndDate_dateInput")).SendKeys(CommonLogic.deliverInvoice);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboClient_Input")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboClient_Input")).SendKeys(CommonLogic.clientNameInvoice);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_btnRefresh")).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//a[contains(text(),'" + orderIdInvoice + "')]/parent::td/parent::tr/td[1]/input[@type='checkbox']")));
            driver.FindElement(By.XPath("//a[contains(text(),'" + orderIdInvoice + "')]/parent::td/parent::tr/td[1]/input[@type='checkbox']")).Click();

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_btnSaveChanges")).Click();
            Thread.Sleep(4000);
        }

        public static void multiBatch(string url, IWebDriver driver)
        {
            var timeout = 10000; // in milliseconds
            var wait = new WebDriverWait(driver, TimeSpan.FromMilliseconds(timeout));

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Invoicing"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Client Invoicing"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Multi Batch Invoice"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_cboClient_Input")));
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboClient_Input")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboClient_Input")).SendKeys(CommonLogic.clientNameInvoice);
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//li[@class='rcbHovered']"))).Click();

            Thread.Sleep(2000);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_rdiStartDate_dateInput")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_rdiStartDate_dateInput")).SendKeys(CommonLogic.collectInvoice);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_rdiEndDate_dateInput")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_rdiEndDate_dateInput")).SendKeys(CommonLogic.deliverInvoice);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_btnRefresh")).Click();

            driver.FindElement(By.XPath("//a[contains(text(),'" + orderIdInvoice + "')]/parent::td/parent::tr/td[1]/input[@type='checkbox']")).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//a[contains(text(),'" + orderIdInvoice + "')]/parent::td/parent::tr/td[12]/input[@type='text']")));
            batchId = driver.FindElement(By.XPath("//a[contains(text(),'" + orderIdInvoice + "')]/parent::td/parent::tr/td[12]/input[@type='text']")).GetAttribute("value");

            Thread.Sleep(2000);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_Button1")).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//td[contains(text(),'" + batchId + "')]/parent::tr/td[14]/a"))).Click();
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            CommonLogic.preInvoiceWindow = driver.WindowHandles.Last();
            Thread.Sleep(4000);
            driver.Close();
            driver.SwitchTo().Window(CommonLogic.orderWindow);

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//td[contains(text(),'" + batchId + "')]/parent::tr/td[2]/span/span/input[@type='checkbox']"))).Click();

            driver.FindElement(By.XPath("//td[contains(text(),'" + batchId + "')]/parent::tr/td[19]/span/input[@type='radio']")).Click();

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_approvalProcess_btnApply")).Click();
            Thread.Sleep(4000);

            Assert.IsTrue(driver.FindElement(By.XPath("//div[contains(text(),'Your request is being actioned.')]")).Displayed);
        }

        public static void multiBatchSubcontracted(string url, IWebDriver driver)
        {
            var timeout = 10000; // in milliseconds
            var wait = new WebDriverWait(driver, TimeSpan.FromMilliseconds(timeout));

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Invoicing"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Client Invoicing"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Multi Batch Invoice"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_cboClient_Input")));

            driver.FindElement(By.XPath("//td[contains(text(),'" + subcontractorName + "')]/parent::tr/td[14]/a")).Click();

            driver.SwitchTo().Window(driver.WindowHandles.Last());
            CommonLogic.preInvoiceWindow = driver.WindowHandles.Last();
            Thread.Sleep(4000);
            driver.Close();
            preInvoiceWindow = "";
            driver.SwitchTo().Window(CommonLogic.orderWindow);

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//td[contains(text(),'" + subcontractorName + "')]/parent::tr/td[2]/span/span/input[@type='checkbox']"))).Click();

            driver.FindElement(By.XPath("//td[contains(text(),'" + subcontractorName + "')]/parent::tr/td[19]/span/input[@type='radio']")).Click();

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_approvalProcess_btnApply")).Click();
            Thread.Sleep(4000);
        }

        public static void deleteSubcontractor(string url, IWebDriver driver)
        {
            var timeout = 25000; // in milliseconds
            var wait = new WebDriverWait(driver, TimeSpan.FromMilliseconds(timeout));

            driver.SwitchTo().Window(CommonLogic.mainWindow);

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Sub-Contractors"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("List Sub-Contractors"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_grdOrganisations_ctl00_ctl02_ctl00_txtSearch"))).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_grdOrganisations_ctl00_ctl02_ctl00_txtSearch")).SendKeys(subcontractorName);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_grdOrganisations_ctl00_ctl02_ctl00_btnSearch")).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//a[contains(text(),'"+ subcontractorName +"')]"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_chkSuspended"))).Click();

            Assert.IsTrue(driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_chkSuspended")).Selected);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_btnAdd")).Click();

            Thread.Sleep(5000);

            Assert.IsTrue(driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_lblConfirmation")).Displayed);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_btnListClients")).Click();

            driver.FindElement(By.Id("filterOptionsDiv")).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_chkShowDeleted"))).Click();

            Thread.Sleep(5000);

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_grdOrganisations_ctl00_ctl02_ctl00_txtSearch"))).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_grdOrganisations_ctl00_ctl02_ctl00_txtSearch")).SendKeys(subcontractorName);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_grdOrganisations_ctl00_ctl02_ctl00_btnSearch")).Click();

            Assert.IsTrue(driver.FindElement(By.XPath("//a[contains(text(),'" + subcontractorName + "')]")).Displayed);
        }

        public static void deleteUser(string url, IWebDriver driver)
        {
            var timeout = 10000; // in milliseconds
            var wait = new WebDriverWait(driver, TimeSpan.FromMilliseconds(timeout));

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Administration"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Users"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("List Users"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Access:'])[1]/following::input[1]"))).Clear();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Access:'])[1]/following::input[1]")).SendKeys(CommonLogic.username);
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='" + CommonLogic.username + "'])[1]/following::button[1]")).Click();
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("btnDelete"))).Click();
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("/html/body/div[1]/div/div/div[2]/button[1]"))).Click();

            Assert.IsTrue(!driver.FindElement(By.XPath("//td[contains(text(),'usernamefqxps')]")).Displayed);
        }

        public static void addOrderFromDeliveryScreen(string url, IWebDriver driver)
        {
            var timeout = 10000; // in milliseconds
            var wait = new WebDriverWait(driver, TimeSpan.FromMilliseconds(timeout));

            var currentWindow = driver.CurrentWindowHandle;
            IList<string> allWindowHandles = driver.WindowHandles;
            if (allWindowHandles.Count > 2)

            {
                driver.SwitchTo().Window(orderWindow);
                driver.SwitchTo().Frame(0);
                orderIdInvoice = driver.FindElement(By.Id("lblOrderID")).Text;
                driver.Close();
                orderWindow = "";
            }

            else
            {

                driver.SwitchTo().Window(mainWindow);
                driver.SwitchTo().Frame(0);
                orderIdInvoice = driver.FindElement(By.Id("lblOrderID")).Text;
                //TODO - find a way to click this close button
                // this does not work
                //driver.FindElement(By.XPath("//button[contains(text(),'Close Window')]")).Click();
            }

            driver.SwitchTo().Window(mainWindow);

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Orders"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Deliveries"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("filterOptionsDiv"))).Click();

            var dateFrom = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_dteStartDate_dateInput"));
            dateFrom.Clear();
            dateFrom.SendKeys(collectInvoice);

            var dateTo = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_dteEndDate_dateInput"));
            dateTo.Clear();
            dateTo.SendKeys(deliverInvoice);

            var clientName = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboClient_Input"));
            clientName.Clear();
            clientName.SendKeys(clientNameInvoice);
            Thread.Sleep(4000);
            clientName.SendKeys(Keys.Enter);

            Thread.Sleep(2000);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_chkShowAll")).Click();

            Thread.Sleep(2000);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_chkSelectAllTrafficAreas")).Click();

            Thread.Sleep(2000);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_btnRefresh")).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//a[contains(text(),'" + orderIdInvoice + "')]/parent::span/parent::td/parent::tr/td[2]/span/input[@type='checkbox']"))).Click();

            driver.FindElement(By.Id("ui-id-2")).Click();

            if (driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_chkTrunkCollection")).Selected)
            {
                driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_chkTrunkCollection")).Click();
            }

            driver.FindElement(By.Id("ui-id-3")).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_ucResource_cboDriver_Input")));
            IWebElement enterDriver = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucResource_cboDriver_Input"));
            enterDriver.SendKeys(displayName);
            Thread.Sleep(2000);
            enterDriver.SendKeys(Keys.Down);
            driver.FindElement(By.ClassName("rcbHovered")).Click();

            IWebElement enterVehicle = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucResource_cboVehicle_Input"));
            if (enterVehicle != null)
            {
                enterVehicle.GetAttribute("value");
            }
            else
            {
                enterVehicle.SendKeys(displayName);
                Thread.Sleep(2000);
                enterVehicle.SendKeys(Keys.Down);
                driver.FindElement(By.ClassName("rcbHovered")).Click();
            }

            IWebElement enterTrailer = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucResource_cboTrailer_Input"));
            enterTrailer.SendKeys(displayName);
            Thread.Sleep(2000);
            enterTrailer.SendKeys(Keys.Down);
            driver.FindElement(By.ClassName("rcbHovered")).Click();

            if (!driver.FindElement(By.Id("chkShowJobOnCreation")).Selected)
            {
                driver.FindElement(By.Id("chkShowJobOnCreation")).Click();
            }

            if (!driver.FindElement(By.Id("chkShowInProgress")).Selected)
            {
                driver.FindElement(By.Id("chkShowInProgress")).Click();
            }

            driver.FindElement(By.Id("btnCreateDeliveryJob")).Click();

            CommonLogic.runWindow = driver.WindowHandles.Last();
            Thread.Sleep(4000);

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_ctl20_repOH_ctl01_btnRecordCallIn"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_btnAddMoveNext"))).Click();

            Thread.Sleep(5000);

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_btnAddMoveNext"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_lblJobId")));
            runIdInvoice = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_lblJobId")).Text;

            driver.Close();

            runWindow = "";

            driver.SwitchTo().Window(mainWindow);

            Thread.Sleep(4000);
        }

    }
}