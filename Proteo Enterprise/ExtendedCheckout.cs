﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium.Interactions;
using System.Net.Http;

namespace SeleniumTests
{
    [TestFixture]
    public class ExtendedCheckout
    {
        private IWebDriver driver;
        private StringBuilder verificationErrors;
        private string baseURL;
        private bool acceptNextAlert = true;
        private string testurl = "test.firmin.proteoenterprise.co.uk";
        private List<string> urlList;
        private String orderId;
        private String runIdInvoice;


        [SetUp]
        public void SetupTest()
        {
            verificationErrors = new StringBuilder();

            urlList = new List<string>();
            //urlList.Add("test.chiltern.proteoenterprise.co.uk");
            //urlList.Add("test.demo.proteoenterprise.co.uk");
            //urlList.Add("test.fandw.proteoenterprise.co.uk");
            urlList.Add("test.firmin.proteoenterprise.co.uk");
            //urlList.Add("test.jr.proteoenterprise.co.uk");
            //urlList.Add("test.kersey.proteoenterprise.co.uk");
            //urlList.Add("test.knowles.proteoenterprise.co.uk");
            //urlList.Add("test.nicholls.proteoenterprise.co.uk");
            //urlList.Add("test.williams.proteoenterprise.co.uk");
            //urlList.Add("test.woodall.proteoenterprise.co.uk");
        }



        [TearDown]
        public void TeardownTest()
        {
            try
            {
                driver.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
            Assert.AreEqual("", verificationErrors.ToString());
        }


        [Test]
        public void ExtendedCheckoutTest()
        {
            foreach (var s in urlList)
            {
                try
                {
                    theTest(s);
                }
                catch (Exception ex)
                {

                    if (CommonLogic.addedDriver == true)
                    {
                        driver.SwitchTo().Window(CommonLogic.mainWindow);

                        CommonLogic.deleteDriver(CommonLogic.displayName, driver);
                    }

                    if (CommonLogic.addedVehicle == true)
                    {
                        driver.Navigate().Refresh();

                        CommonLogic.deleteVehicle(CommonLogic.vehicleName, driver);
                    }

                    if (CommonLogic.addedTrailer == true)
                    {
                        driver.Navigate().Refresh();

                        CommonLogic.deleteTrailer(CommonLogic.trailerRef, driver);
                    }

                    if (CommonLogic.addedClient == true)
                    {
                        driver.Navigate().Refresh();

                        CommonLogic.deleteClient(CommonLogic.clientName, driver);
                    }

                    if (CommonLogic.addedControlArea == true)
                    {
                        driver.SwitchTo().Window(CommonLogic.orderWindow);

                        CommonLogic.unassignFromControlArea(CommonLogic.clientName, driver);
                    }

                    else
                    {
                        throw new Exception(s + " failed!,  " + ex.Message.ToString());
                    }
                }
            }
        }

        private void theTest(string url)
        {
            driver = new ChromeDriver("C:\\webdrivers");
            CommonLogic.login(url, driver);

            CommonLogic.addClient(url, driver);

            addExtra(url, driver);

            CommonLogic.storeControlAreas(url, driver);

            CommonLogic.assignToControlArea(url, driver);

            CommonLogic.addOrder(url, driver);

            dragRunFromTrafficSheet(url);

        }

        private void addNewPoint(string url)
        {
            var timeout = 10000; // in milliseconds
            var wait = new WebDriverWait(driver, TimeSpan.FromMilliseconds(timeout));

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Clients/Client Customers"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Add New Point"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_cboClient_Input"))).Clear();

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboClient_Input")).SendKeys(CommonLogic.clientName);
            Thread.Sleep(2000);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboClient_Input")).SendKeys(Keys.Enter);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtAddressLine1")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtAddressLine1")).SendKeys(CommonLogic.address);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtAddressLine2")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtAddressLine2")).SendKeys(CommonLogic.address);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtAddressLine3")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtAddressLine3")).SendKeys(CommonLogic.address);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtPostTown")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtPostTown")).SendKeys(CommonLogic.address);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtCounty")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtCounty")).SendKeys(CommonLogic.address);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtPointNotes")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtPointNotes")).SendKeys(CommonLogic.pointNotes);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboTrafficArea")).Click();
            new SelectElement(driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboTrafficArea"))).SelectByValue(CommonLogic.selectedTrafficArea1);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_btnAdd")).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_cboClient_Input"))).Clear();
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_cboClient_Input"))).SendKeys(CommonLogic.clientName);
            Thread.Sleep(2000);
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_cboClient_Input"))).SendKeys(Keys.Enter);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_btnRefresh")).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//h3[contains(text(),'Points for Organisation')]")));

            Assert.IsTrue(driver.FindElement(By.XPath("//a[contains(text(),'"+ CommonLogic.clientName +"')]")).Displayed);

            newUser(url);
        }

        private void newUser(string url)
        {
            var timeout = 10000; // in milliseconds
            var wait = new WebDriverWait(driver, TimeSpan.FromMilliseconds(timeout));

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Administration"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Users"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("List Users"))).Click();

            Thread.Sleep(2000);

            driver.FindElement(By.CssSelector("[ng-click='addUser()']")).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Name("username"))).Clear();
            driver.FindElement(By.Name("username")).SendKeys(CommonLogic.username);
            driver.FindElement(By.Name("firstname")).Clear();
            driver.FindElement(By.Name("firstname")).SendKeys(CommonLogic.firstName);
            driver.FindElement(By.Name("lastname")).Clear();
            driver.FindElement(By.Name("lastname")).SendKeys(CommonLogic.lastName);
            driver.FindElement(By.Name("password")).Clear();
            driver.FindElement(By.Name("password")).SendKeys(CommonLogic.password);
            driver.FindElement(By.Name("confirm")).Clear();
            driver.FindElement(By.Name("confirm")).SendKeys(CommonLogic.password);
            driver.FindElement(By.Name("email")).Clear();
            driver.FindElement(By.Name("email")).SendKeys(CommonLogic.emailAddress);
            SelectElement selectSecurityRoles = new SelectElement(driver.FindElement(By.XPath(".//*[@id='bootstrap-duallistbox-nonselected-list_']")));
            selectSecurityRoles.SelectByText("Planner");
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='show all'])[1]/following::button[2]")).Click();
            Thread.Sleep(2000);
            driver.FindElement(By.Id("btnSave")).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.CssSelector("[ng-model='searchUser']"))).Clear();
            driver.FindElement(By.CssSelector("[ng-model='searchUser']")).SendKeys(CommonLogic.username);
            Thread.Sleep(5000);
            Assert.IsTrue(driver.FindElement(By.XPath("//td[contains(text(),'" + CommonLogic.emailAddress + "')]")).Displayed);

            addTrailer(url);
        }


        private void addTrailer(string url)
        {
            var timeout = 20000; // in milliseconds
            var wait = new WebDriverWait(driver, TimeSpan.FromMilliseconds(timeout));

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Resources"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Trailers"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("View All"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_grdTrailers_ctl00_ctl02_ctl00_btnAddNewTrailer"))).Click();

            driver.SwitchTo().Window(driver.WindowHandles.Last());

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtTrailerRef")).Click();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtTrailerRef")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtTrailerRef")).SendKeys(CommonLogic.trailerRef);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtDisplayName")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtDisplayName")).SendKeys(CommonLogic.displayName);
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Trailer Manufacturer'])[1]/following::td[1]")).Click();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboTrailerManufacturer")).Click();
            new SelectElement(driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboTrailerManufacturer"))).SelectByText("Comet/Southfields");
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboTrailerManufacturer")).Click();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboTrailerType")).Click();
            new SelectElement(driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboTrailerType"))).SelectByText("13.6M High Tautliner");
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboTrailerType")).Click();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboTrailerDescription")).Click();
            new SelectElement(driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboTrailerDescription"))).SelectByText("High");
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboTrailerDescription")).Click();

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboOrganisation_Input")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboOrganisation_Input")).Click();

            IWebElement enterOrganisation = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboOrganisation_Input"));
            enterOrganisation.SendKeys(CommonLogic.clientName);
            Thread.Sleep(3000);
            enterOrganisation.SendKeys(Keys.Enter);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboPoint_Input")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboPoint_Input")).Click();

            IWebElement enterPoint = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboPoint_Input"));
            enterPoint.SendKeys(CommonLogic.clientName);
            Thread.Sleep(3000);
            enterPoint.SendKeys(Keys.Enter);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboControlArea")).Click();
            new SelectElement(driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboControlArea"))).SelectByText("Northwich");
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboControlArea")).Click();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboTrafficArea")).Click();
            new SelectElement(driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboTrafficArea"))).SelectByText("East Anglia");
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboTrafficArea")).Click();

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_btnAdd")).Click();

            driver.SwitchTo().Window(driver.WindowHandles.Last());

            driver.FindElement(By.Id("qs777585")).Clear();

            driver.FindElement(By.Id("qs777585")).SendKeys(CommonLogic.displayName);
    
            Thread.Sleep(2000);

            Assert.IsTrue(driver.FindElement(By.XPath("//td[contains(text(),'" + CommonLogic.displayName + "')]")).Displayed);

            CommonLogic.addedTrailer = true;

            addVehicle(url);
        }

        private void addVehicle(string url)
        {
            var wait = new WebDriverWait(driver, new TimeSpan(0, 0, 30));

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Resources"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Vehicles"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("View All"))).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//*[@class='pull-right']//*[text()='Add Vehicle']"))).Click();

            Thread.Sleep(5000);

            driver.SwitchTo().Window(driver.WindowHandles.Last());

            driver.FindElement(By.Id("VehicleName")).Clear();
            driver.FindElement(By.Id("VehicleName")).SendKeys(CommonLogic.vehicleName);
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Registration No'])[1]/following::input[1]")).Clear();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Registration No'])[1]/following::input[1]")).SendKeys(CommonLogic.vehicleRegNo);
            driver.FindElement(By.Name("manufacturer")).Click();
            new SelectElement(driver.FindElement(By.Name("manufacturer"))).SelectByText("DAF");
            driver.FindElement(By.Name("manufacturer")).Click();
            new SelectElement(driver.FindElement(By.Name("model"))).SelectByText("95XF");
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Chassis No'])[1]/following::input[1]")).Clear();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Chassis No'])[1]/following::input[1]")).SendKeys("test");
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Cab Phone Number'])[1]/following::input[1]")).Clear();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Cab Phone Number'])[1]/following::input[1]")).SendKeys("test");
            driver.FindElement(By.Name("class")).Click();
            new SelectElement(driver.FindElement(By.Name("class"))).SelectByText("Class 1");
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='MOT Expires On'])[1]/following::button[1]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Sat'])[1]/following::span[11]")).Click();
            driver.FindElement(By.Id("nextservicedate")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Next Service Date'])[1]/following::button[1]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Sat'])[1]/following::button[18]")).Click();
            driver.FindElement(By.Name("vehicleType")).Click();
            new SelectElement(driver.FindElement(By.Name("vehicleType"))).SelectByText("Test");
            driver.FindElement(By.Name("vehicleType")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Is ToughTouch Installed'])[1]/following::div[4]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Nominal Code'])[1]/following::select[1]")).Click();
            new SelectElement(driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Nominal Code'])[1]/following::select[1]"))).SelectByText("00000 - Default");
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Nominal Code'])[1]/following::select[1]")).Click();
            driver.FindElement(By.Name("vehicledepot")).Click();
            new SelectElement(driver.FindElement(By.Name("vehicledepot"))).SelectByText("Northwich Depot");
            driver.FindElement(By.Name("vehicledepot")).Click();
            new SelectElement(driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='This vehicle is a'])[1]/following::select[1]"))).SelectByText("Northwich");
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='This vehicle is a'])[1]/following::select[1]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='This vehicle is a'])[1]/following::select[2]")).Click();
            new SelectElement(driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='This vehicle is a'])[1]/following::select[2]"))).SelectByText("East Anglia");
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='This vehicle is a'])[1]/following::select[2]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Dedicated to client'])[1]/following::input[1]")).Clear();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Dedicated to client'])[1]/following::input[1]")).SendKeys("proteo");
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Dedicated to client'])[1]/following::div[2]")).Click();

            IWebElement enterOrganisation = driver.FindElement(By.CssSelector("[ng-model='vehicle.point.organizationName']"));
            enterOrganisation.SendKeys(CommonLogic.clientName);
            Thread.Sleep(3000);
            enterOrganisation.SendKeys(Keys.Enter);

            IWebElement enterPoint = driver.FindElement(By.CssSelector("[ng-model='vehicle.point.companyName']"));
            enterPoint.SendKeys(CommonLogic.clientName);
            Thread.Sleep(3000);
            enterPoint.SendKeys(Keys.Enter);

            Thread.Sleep(2000);

            driver.FindElement(By.CssSelector("[ng-click='saveVehicle()']")).Click();

            Thread.Sleep(5000);

            addDriver(url);
        }

        // Adding a Driver is a seperate test here because we create a new User prior to this and set them as the Planner for that Driver in that test

        private void addDriver(string url)
        {
            var timeout = 10000; // in milliseconds
            var wait = new WebDriverWait(driver, TimeSpan.FromMilliseconds(timeout));

            driver.FindElement(By.XPath("//*[@class='rmRootGroup rmHorizontal']//*[text()='Resources']")).Click();

            Thread.Sleep(2000);

            driver.FindElement(By.XPath("//*[@class='rmItem rmFirst']//*[text()='Drivers']")).Click();

            Thread.Sleep(2000);

            driver.FindElement(By.XPath("//*[@class='rmLink']//*[text()='View All']")).Click();

            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Send Message'])[1]/following::button[1]")).Click();

            Thread.Sleep(2000);

            SelectElement titleDropdown = new SelectElement(driver.FindElement(By.CssSelector("[ng-model='driver.individual.titleId']")));
            titleDropdown.SelectByText("Mr");
            driver.FindElement(By.Id("firstnames")).Clear();
            driver.FindElement(By.Id("firstnames")).SendKeys(CommonLogic.firstName);
            driver.FindElement(By.Id("lastname")).Clear();
            driver.FindElement(By.Id("lastname")).SendKeys(CommonLogic.lastName);
            driver.FindElement(By.Id("displayName")).Clear();
            driver.FindElement(By.Id("displayName")).SendKeys(CommonLogic.displayName);
            driver.FindElement(By.Id("passcode")).Clear();
            driver.FindElement(By.Id("passcode")).SendKeys(CommonLogic.driverPasscode);
            new SelectElement(driver.FindElement(By.Name("drivertype"))).SelectByText("Test");
            driver.FindElement(By.Id("posttown")).Click();
            driver.FindElement(By.Id("posttown")).Clear();
            driver.FindElement(By.Id("posttown")).SendKeys(CommonLogic.postalTown);
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Usual Vehicle'])[2]/following::select[1]")).Click();
            new SelectElement(driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Usual Vehicle'])[2]/following::select[1]"))).SelectByText(CommonLogic.vehicleName);
            Thread.Sleep(2000);
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Usual Vehicle'])[2]/following::select[1]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)=concat('Driver', \"'\", 's Depot')])[1]/following::select[1]")).Click();
            new SelectElement(driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)=concat('Driver', \"'\", 's Depot')])[1]/following::select[1]"))).SelectByText("Northwich Depot");
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)=concat('Driver', \"'\", 's Depot')])[1]/following::select[1]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='This driver is a'])[1]/following::select[1]")).Click();
            new SelectElement(driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='This driver is a'])[1]/following::select[1]"))).SelectByText("Northwich");
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='This driver is a'])[1]/following::select[1]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='This driver is a'])[1]/following::select[2]")).Click();
            new SelectElement(driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='This driver is a'])[1]/following::select[2]"))).SelectByText("East Anglia");
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='This driver is a'])[1]/following::select[2]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='This driver is a'])[1]/following::select[1]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Planner'])[1]/following::select[1]")).Click();
            new SelectElement(driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Planner'])[1]/following::select[1]"))).SelectByText(CommonLogic.plannerName);
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Planner'])[1]/following::select[1]")).Click();
            driver.FindElement(By.Id("mobilephone")).Click();
            driver.FindElement(By.Id("mobilephone")).Clear();
            driver.FindElement(By.Id("mobilephone")).SendKeys(CommonLogic.mobilePhone);
            driver.FindElement(By.Id("workemail")).Clear();
            driver.FindElement(By.Id("workemail")).SendKeys(CommonLogic.emailAddress);
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Pref. Communication'])[1]/following::select[1]")).Click();
            new SelectElement(driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Pref. Communication'])[1]/following::select[1]"))).SelectByText("ProteoMobile");
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Pref. Communication'])[1]/following::select[1]")).Click();

            IWebElement enterOrganisation = driver.FindElement(By.CssSelector("[ng-model='driver.point.organizationName']"));
            enterOrganisation.SendKeys(CommonLogic.clientName);
            Thread.Sleep(3000);
            enterOrganisation.SendKeys(Keys.Enter);

            IWebElement enterPoint = driver.FindElement(By.CssSelector("[ng-model='driver.point.companyName']"));
            enterPoint.SendKeys(CommonLogic.clientName);
            Thread.Sleep(3000);
            enterPoint.SendKeys(Keys.Enter);
            Thread.Sleep(3000);

            driver.FindElement(By.CssSelector("[ng-click='saveDriver()']")).Click();
            Thread.Sleep(5000);

            addReference(url);
        }


        private void addReference(string url)
        {
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='View Fleet'])[1]/following::span[1]")).Click();
            Thread.Sleep(2000);
            driver.FindElement(By.LinkText("List Clients")).Click();
            Thread.Sleep(2000);
            driver.Navigate().Refresh();
            Thread.Sleep(2000);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_grdOrganisations_ctl00_ctl02_ctl00_txtSearch")).Click();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_grdOrganisations_ctl00_ctl02_ctl00_txtSearch")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_grdOrganisations_ctl00_ctl02_ctl00_txtSearch")).SendKeys(CommonLogic.clientName);
            Thread.Sleep(2000);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_grdOrganisations_ctl00_ctl02_ctl00_txtSearch")).SendKeys(Keys.Enter);
            Thread.Sleep(5000);
            driver.FindElement(By.LinkText(CommonLogic.clientName)).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='D'])[1]/following::span[3]")).Click();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_btnAddReference")).Click();
            acceptNextAlert = true;
            CloseAlertAndGetItsText();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtDescription")).Click();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtDescription")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtDescription")).SendKeys(CommonLogic.referenceName);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboDataType")).Click();
            new SelectElement(driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboDataType"))).SelectByText("Free Text");
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboDataType")).Click();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboStatus")).Click();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboStatus")).Click();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_chkIsMandatoryOnOrder")).Click();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_btnAdd")).Click();

            addTarrif(url);
        }

        private string CloseAlertAndGetItsText()
        {
            try
            {
                IAlert alert = driver.SwitchTo().Alert();
                string alertText = alert.Text;
                if (acceptNextAlert)
                {
                    alert.Accept();
                }
                else
                {
                    alert.Dismiss();
                }
                return alertText;
            }
            finally
            {
                acceptNextAlert = true;
            }
        }

        private void addTarrif(string url)
        {
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Lookup Sub-Contractor Contact'])[1]/following::span[1]")).Click();
            Thread.Sleep(2000);
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Manage My Filters'])[1]/following::span[2]")).Click();
            Thread.Sleep(2000);
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='List Scales'])[1]/following::span[1]")).Click();
            Thread.Sleep(2000);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_btnAdd")).Click();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtTariffDescription")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtTariffDescription")).SendKeys(CommonLogic.tarrifDescription);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtVersionDescription")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Increase Tariff Table Rates By'])[1]/following::label[1]")).Click();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboZoneMap_Input")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Jiffy Zone Map'])[1]/following::li[1]")).Click();
            Thread.Sleep(2000);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_btnSave")).Click();
            Thread.Sleep(2000);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_dteFinishDate_popupButton")).Click();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_dteFinishDate_calendar_NN")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='S'])[2]/following::a[35]")).Click();
            Thread.Sleep(3000);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtTableDescription")).Click();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtTableDescription")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtTableDescription")).SendKeys(CommonLogic.tableDescription);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_btnSave")).Click();
            Thread.Sleep(2000);

            addOrder(url);
        }

        // Add Order functionality

        private void addOrder(string url)
        {
            addClientPortalUser(url);
        }

        // Add a new Client Portal user

        private void addClientPortalUser(string url)
        {
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Lookup Sub-Contractor Contact'])[1]/following::span[1]")).Click();
            Thread.Sleep(2000);
            driver.FindElement(By.LinkText("Users")).Click();
            Thread.Sleep(2000);
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='User Group Access Control'])[1]/following::span[1]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Remote Assistance'])[1]/following::input[2]")).Click();
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboClient_Input")).Click();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboClient_Input")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboClient_Input")).SendKeys(CommonLogic.clientName);
            Thread.Sleep(5000);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboClient_Input")).SendKeys(Keys.Enter);
            Thread.Sleep(2000);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtUserName")).Click();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtUserName")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtUserName")).SendKeys(CommonLogic.clientUsername);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtForenames")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtForenames")).SendKeys(CommonLogic.firstName);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtSurname")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtSurname")).SendKeys(CommonLogic.lastName);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtPassword")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtPassword")).SendKeys(CommonLogic.password);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtConfirmPassword")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtConfirmPassword")).SendKeys(CommonLogic.password);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtEmail")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtEmail")).SendKeys(CommonLogic.emailAddress);
            Thread.Sleep(2000);
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='User Groups'])[1]/following::option[3]")).Click();
            driver.FindElement(By.Name("add")).Click();
            Thread.Sleep(2000);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_chkEmailDetails")).Click();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_btnAdd")).Click();
            Thread.Sleep(5000);
            driver.SwitchTo().Window(driver.WindowHandles.Last());

            addOrderFromClientPortal(url);
        }

        private void addOrderFromClientPortal(string url)
        {
            // TODO - make this url dynamic based on the client site
            driver.Navigate().GoToUrl("https://test.client.firmin.proteoenterprise.co.uk/login");
            Thread.Sleep(5000);
            driver.FindElement(By.Name("username")).Clear();
            driver.FindElement(By.Name("username")).SendKeys(CommonLogic.clientUsername);
            driver.FindElement(By.Name("password")).Clear();
            driver.FindElement(By.Name("password")).SendKeys(CommonLogic.password);
            driver.FindElement(By.Id("loginButton")).Click();
            Thread.Sleep(2000);
            driver.FindElement(By.Name("oldpassword")).Click();
            driver.FindElement(By.Name("oldpassword")).Clear();
            driver.FindElement(By.Name("oldpassword")).SendKeys(CommonLogic.password);
            driver.FindElement(By.Name("confirmpassword")).Click();
            driver.FindElement(By.Name("newpassword")).Click();
            driver.FindElement(By.Name("newpassword")).Clear();
            driver.FindElement(By.Name("newpassword")).SendKeys(CommonLogic.newPassword);
            driver.FindElement(By.Name("confirmpassword")).Click();
            driver.FindElement(By.Name("confirmpassword")).Clear();
            driver.FindElement(By.Name("confirmpassword")).SendKeys(CommonLogic.newPassword);
            driver.FindElement(By.Id("changeButton")).Click();
            Thread.Sleep(2000);
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Update'])[1]/following::button[1]")).Click();
            Thread.Sleep(5000);
            Assert.AreEqual("Add an Order", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Log Off'])[1]/following::h2[1]")).Text);
            Thread.Sleep(2000);
            driver.FindElement(By.Name("collectPoint")).Click();
            driver.FindElement(By.Name("collectPoint")).Clear();
            driver.FindElement(By.Name("collectPoint")).SendKeys("proteo");
            Thread.Sleep(3000);
            driver.FindElement(By.Name("collectPoint")).SendKeys(Keys.Enter);
            Thread.Sleep(2000);
            driver.FindElement(By.Name("deliveryPoint")).Click();
            driver.FindElement(By.Name("deliveryPoint")).Clear();
            driver.FindElement(By.Name("deliveryPoint")).SendKeys(CommonLogic.clientName);
            Thread.Sleep(3000);
            driver.FindElement(By.Name("deliveryPoint")).SendKeys(Keys.Enter);
            Thread.Sleep(2000);
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Total Rate'])[1]/following::span[1]")).Click();
            // TODO: if references method has been run, then execute the below code
            driver.FindElement(By.Name("additionalReferenceField")).Click();
            driver.FindElement(By.Name("additionalReferenceField")).Clear();
            driver.FindElement(By.Name("additionalReferenceField")).SendKeys(CommonLogic.referenceName);
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Total Rate'])[1]/following::span[1]")).Click();
            Thread.Sleep(3000);
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Add Paperwork'])[1]/preceding::div[1]")).Click();
            // Todo: save Order ID as a variable here, it's mixed up in text, ie 'Order ID 838458 created' for instance, so need to work out a way to extract it
            //orderId = driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Add Paperwork'])[1]/preceding::div[1]")).Text;
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='View DN'])[1]/following::button[1]")).Click();
            Thread.Sleep(5000);

            verifyCPOrderVisible(url);
        }

        private void verifyCPOrderVisible(string url)
        {
            var dateRaw = DateTime.Now;
            var todaysDate = DateTime.Now.ToString("dd/MM/yyyy");
            var tomorrowsDateRaw = dateRaw.AddDays(1);
            var tomorrowsDate = tomorrowsDateRaw.ToString("dd/MM/yyyy");

            // TODO - make this url dynamic based on the client site
            driver.Navigate().GoToUrl("https://test.firmin.proteoenterprise.co.uk/");
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Device Management'])[1]/following::span[1]")).Click();
            Thread.Sleep(2000);
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Orders'])[3]/following::span[1]")).Click();
            Thread.Sleep(2000);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_dteStartDate_dateInput")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_dteStartDate_dateInput")).SendKeys(todaysDate);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_dteEndDate_dateInput")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_dteEndDate_dateInput")).SendKeys(tomorrowsDate);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboClient_Input")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboClient_Input")).SendKeys(CommonLogic.clientName);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboClient_Input")).SendKeys(Keys.Enter);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_btnSearch")).Click();
            Thread.Sleep(2000);

            try
            {
                Assert.AreEqual(orderId, driver.FindElement(By.LinkText(orderId)).Text);
            }
            catch (AssertionException e)
            {
                verificationErrors.Append(e.Message);
            }

            cancelOrderCP(url);
        }
        private bool IsElementPresent(By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        private bool IsAlertPresent()
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }

        private void cancelOrderCP(string url)
        {
            // TODO - make this url dynamic based on the client site
            driver.Navigate().GoToUrl("https://test.client.firmin.proteoenterprise.co.uk/");
            Thread.Sleep(5000);
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Status'])[2]/following::button[1]")).Click();
            Thread.Sleep(2000);
            driver.FindElement(By.LinkText("Cancel Order")).Click();
            Thread.Sleep(2000);
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Are you sure you want to cancel this order?'])[1]/following::button[1]")).Click();
            //TODO - check that the order ID is no longer visible here
            Thread.Sleep(5000);

            CommonLogic.deleteClient(url,driver);
        }


        private void deleteUser(string url)
        {
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Lookup Sub-Contractor Contact'])[1]/following::span[1]")).Click();
            Thread.Sleep(5000);
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Manage Pallet Types'])[1]/following::span[2]")).Click();
            Thread.Sleep(5000);
            driver.FindElement(By.XPath("//*[@class='rmItem rmFirst']//*[text()='List Users']")).Click();
            Thread.Sleep(3000);
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Access:'])[1]/following::input[1]")).Clear();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Access:'])[1]/following::input[1]")).SendKeys(CommonLogic.username);
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='" + CommonLogic.username + "'])[1]/following::button[1]")).Click();
            Thread.Sleep(3000);
            driver.FindElement(By.Id("btnDelete")).Click();
            Thread.Sleep(3000);
            driver.FindElement(By.XPath("/html/body/div[1]/div/div/div[2]/button[1]")).Click();

            deleteClientUser(url);
        }

        private void deleteClientUser(string url)
        {
            //TODO - need to work out how to make this delete the user relating to the Client User created from the first method - currently this is hardcoded

            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Lookup Sub-Contractor Contact'])[1]/following::span[1]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Manage Pallet Types'])[1]/following::span[2]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='User Group Access Control'])[1]/following::span[1]")).Click();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_grdUsers_ctl00__39__0")).Click();
            driver.FindElement(By.LinkText("Click to Update 'usernamezljyn'")).Click();
            acceptNextAlert = true;
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_btnRemove")).Click();
            Assert.IsTrue(Regex.IsMatch(CloseAlertAndGetItsText(), "^Are you sure you wish to remove this User, as this action cannot be undone once confirmed[\\s\\S]$"));
            driver.Close();

            addExtra(url, driver);
        }

        private void addExtra(string url, IWebDriver driver)
        {
            var timeout = 10000; // in milliseconds
            var wait = new WebDriverWait(driver, TimeSpan.FromMilliseconds(timeout));

            driver.FindElement(By.LinkText("Administration")).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Extra Type")));

            driver.FindElement(By.LinkText("Extra Type")).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_grdExtraTypes_ctl00_ctl03_ctl01_btnAddNewExtraType")));

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_grdExtraTypes_ctl00_ctl03_ctl01_btnAddNewExtraType")).Click();
            Thread.Sleep(2000);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_grdExtraTypes_ctl00_ctl02_ctl01_txtShortDescription")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_grdExtraTypes_ctl00_ctl02_ctl01_txtShortDescription")).SendKeys(CommonLogic.extraShortDescription);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_grdExtraTypes_ctl00_ctl02_ctl01_txtDescription")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_grdExtraTypes_ctl00_ctl02_ctl01_txtDescription")).SendKeys(CommonLogic.extraDescription);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_grdExtraTypes_ctl00_ctl02_ctl01_chkIsEnabled")).Click();

            Assert.IsTrue(driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_grdExtraTypes_ctl00_ctl02_ctl01_chkIsEnabled")).Selected);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_grdExtraTypes_ctl00_ctl02_ctl01_chkIsDisplayedOnAddUpdateOrder")).Click();

            Assert.IsTrue(driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_grdExtraTypes_ctl00_ctl02_ctl01_chkIsDisplayedOnAddUpdateOrder")).Selected);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_grdExtraTypes_ctl00_ctl02_ctl01_PerformInsertButton")).Click();

            Thread.Sleep(2000);

            Assert.IsTrue(driver.FindElement(By.XPath("//div[contains(text(),'" + CommonLogic.extraDescription + "')]")).Displayed);

            driver.FindElement(By.LinkText("Clients/Client Customers")).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("List Clients")));

            driver.FindElement(By.LinkText("List Clients")).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("ctl00_ContentPlaceHolder1_grdOrganisations_ctl00_ctl02_ctl00_txtSearch")));

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_grdOrganisations_ctl00_ctl02_ctl00_txtSearch")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_grdOrganisations_ctl00_ctl02_ctl00_txtSearch")).SendKeys(CommonLogic.clientName);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_grdOrganisations_ctl00_ctl02_ctl00_btnSearch")).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText(CommonLogic.clientName)));

            driver.FindElement(By.LinkText(CommonLogic.clientName)).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='L'])[1]/following::u[1]")).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//td[contains(text(),'" + CommonLogic.extraDescription + "')]")));

            Assert.IsTrue(driver.FindElement(By.XPath("//td[contains(text(),'" + CommonLogic.extraDescription + "')]")).Displayed);

            IWebElement orderCapture = driver.FindElement(By.XPath("//td[contains(text(),'" + CommonLogic.extraDescription + "')]/parent::tr/td[2]/input[@type='checkbox']"));
            orderCapture.Click();

            Assert.IsTrue(driver.FindElement(By.XPath("//td[contains(text(),'" + CommonLogic.extraDescription + "')]/parent::tr/td[2]/input[@type='checkbox']")).Selected);

            IWebElement enabledByDefault = driver.FindElement(By.XPath("//td[contains(text(),'" + CommonLogic.extraDescription + "')]/parent::tr/td[3]/input[@type='checkbox']"));
            enabledByDefault.Click();

            Assert.IsTrue(driver.FindElement(By.XPath("//td[contains(text(),'" + CommonLogic.extraDescription + "')]/parent::tr/td[3]/input[@type='checkbox']")).Selected);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_btnAdd")).Click();

            // Checking whether the extra type is visible on the Order section

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Orders")));

            driver.FindElement(By.LinkText("Orders")).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.PartialLinkText("Add Order")));

            driver.FindElement(By.PartialLinkText("Add Order")).Click();

            CommonLogic.orderWindow = driver.WindowHandles.Last();

            driver.SwitchTo().Window(CommonLogic.orderWindow);

            driver.SwitchTo().Window(driver.WindowHandles.Last());

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id(("ctl00_ContentPlaceHolder1_ucOrder_cboClient_Input"))));

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_cboClient_Input")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_cboClient_Input")).SendKeys(CommonLogic.clientName);

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//li[@class='rcbHovered']")));

            driver.FindElement(By.XPath("//li[@class='rcbHovered']")).Click();

            Thread.Sleep(5000);
                
            try
            {
                Assert.AreEqual(CommonLogic.extraDescription, driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_grdExtras_ctl00_ctl04_lblExtraType")).Text);
            }
            catch (AssertionException e)
            {
                verificationErrors.Append(e.Message);
            }

           // driver.Close();

            driver.SwitchTo().Window(CommonLogic.mainWindow);

            driver.SwitchTo().Window(driver.WindowHandles.Last());

            driver.Navigate().Refresh();

            driver.FindElement(By.LinkText("Administration")).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Extra Type")));

            driver.FindElement(By.LinkText("Extra Type")).Click();
            
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//table[@class='rgMasterTable']")));

            driver.FindElement(By.XPath("//div[contains(text(),'"+ CommonLogic.extraShortDescription +"')]/parent::td/parent::tr/td[1]/a")).Click();
            
            if (driver.FindElement(By.XPath("//input[@value='" + CommonLogic.extraShortDescription + "']/parent::td/parent::tr/parent::tbody/tr[5]/td[2]/input[@type='checkbox']")).Selected)
            {
                driver.FindElement(By.XPath("//input[@value='" + CommonLogic.extraShortDescription + "']/parent::td/parent::tr/parent::tbody/tr[5]/td[2]/input[@type='checkbox']")).Click();
            }

            if (driver.FindElement(By.XPath("//input[@value='" + CommonLogic.extraShortDescription + "']/parent::td/parent::tr/parent::tbody/tr[7]/td[2]/span/input[@type='checkbox']")).Selected)
            {
                driver.FindElement(By.XPath("//input[@value='" + CommonLogic.extraShortDescription + "']/parent::td/parent::tr/parent::tbody/tr[7]/td[2]/span/input[@type='checkbox']")).Click();
            }
            driver.FindElement(By.XPath("//input[@value='" + CommonLogic.extraShortDescription + "']/parent::td/parent::tr/parent::tbody/parent::table/parent::td/parent::tr/parent::tbody/tr[2]/td[1]/input[@value='Update']")).Click();

            Thread.Sleep(2000);

            // Checking whether the extra type is visible on the Order section

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Orders")));

            driver.FindElement(By.LinkText("Orders")).Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.PartialLinkText("Add Order")));

            driver.FindElement(By.PartialLinkText("Add Order")).Click();

            CommonLogic.orderWindow = driver.WindowHandles.Last();

            driver.SwitchTo().Window(CommonLogic.orderWindow);

            driver.SwitchTo().Window(driver.WindowHandles.Last());

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id(("ctl00_ContentPlaceHolder1_ucOrder_cboClient_Input"))));

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_cboClient_Input")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_cboClient_Input")).SendKeys(CommonLogic.clientName);

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//li[@class='rcbHovered']")));

            driver.FindElement(By.XPath("//li[@class='rcbHovered']")).Click();

            Thread.Sleep(5000);

            try
            {
                Assert.AreNotEqual(CommonLogic.extraDescription, driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_grdExtras_ctl00_ctl04_lblExtraType")).Text);
            }
            catch (AssertionException e)
            {
                verificationErrors.Append(e.Message);
            }

            // remoeved due to problem with incomplete clients
            //CommonLogic.deleteClient(url, driver);
        }



        private void resourceUnits(string url)
        {
            driver.FindElement(By.LinkText("Planning")).Click();
            Thread.Sleep(2000);
            driver.FindElement(By.LinkText("Resource Units")).Click();

            CommonLogic.resourceUnitsWindow = driver.WindowHandles.Last();

            driver.SwitchTo().Window(CommonLogic.resourceUnitsWindow);

            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Drivers'])[1]/preceding::button[1]")).Click();
            Thread.Sleep(2000);
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Drivers'])[1]/following::label[2]")).Click();
            Thread.Sleep(2000);
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Showing another planners drivers'])[1]/following::div[7]")).Click();

            Thread.Sleep(5000);
      
            IWebElement sourceElement = driver.FindElement(By.XPath("(//ul[@class='list-group'])[1]/following::li[1]"));
            IWebElement targetElement = driver.FindElement(By.XPath("(//td[@class='dhx_matrix_cell '])[1]"));

            Actions action = new Actions(driver);
            action.ClickAndHold(sourceElement).MoveToElement(targetElement).Perform();

            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Edit'])[6]/following::button[1]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Are you sure you want to delete this resource unit?'])[1]/following::button[1]")).Click();
            driver.FindElement(By.LinkText("Scheduler View (All Resource Units)")).Click();
            driver.FindElement(By.LinkText("List View")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='DA11 JUT(R'])[2]/following::button[1]")).Click();
            driver.FindElement(By.Id("driver")).Clear();
            driver.FindElement(By.Id("driver")).SendKeys("geo");
            driver.FindElement(By.Name("addEditResourceUnitForm")).Submit();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Delete'])[1]/following::button[1]")).Click();
            driver.FindElement(By.LinkText("Scheduler View (All Resource Units)")).Click();

            changeBookedandPlannedTimes(url);
        }

        private void dragRunFromTrafficSheet(string url)
        {

          

            CommonLogic.runWindow = driver.WindowHandles.Last();

            driver.SwitchTo().Window(driver.WindowHandles.Last());

            runIdInvoice = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_lblJobId")).Text;

            driver.Close();

            CommonLogic.runWindow = "";

            if (CommonLogic.orderWindow != "")
            {
                driver.SwitchTo().Window(driver.WindowHandles.Last());

                CommonLogic.orderWindow = driver.WindowHandles.Last();

                CommonLogic.orderWindow = "";

                //driver.Close();
            }

            driver.SwitchTo().Window(CommonLogic.mainWindow);

            //driver.Navigate().Refresh();

            Thread.Sleep(2000);

            driver.FindElement(By.LinkText("Run")).Click();

            Thread.Sleep(2000);

            driver.FindElement(By.LinkText("Traffic Sheet")).Click();

            Thread.Sleep(5000);

            driver.FindElement(By.LinkText("Current Window")).Click();

            Thread.Sleep(5000);

            driver.SwitchTo().Frame(1);

            driver.FindElement(By.XPath("//table[@id='filters']/tbody/tr/td[3]/img[1]")).Click();

            Thread.Sleep(1000);

            CommonLogic.trafficSheetWindow = driver.WindowHandles.Last();

            driver.SwitchTo().Window(CommonLogic.trafficSheetWindow);

            IWebElement selectedControlArea = driver.FindElement(By.XPath("//table[@id='cboControlArea']/tbody/tr/td/label[contains(text(), '" + CommonLogic.selectedControlArea + "')]/parent::td/input[@type='radio']"));
            selectedControlArea.Click();

            if (!driver.FindElement(By.Id("chkSelectAllTrafficAreas")).Selected)
            {
                driver.FindElement(By.Id("chkSelectAllTrafficAreas")).Click();

            } else
            {
                driver.FindElement(By.Id("chkSelectAllTrafficAreas")).Click();

                driver.FindElement(By.Id("chkSelectAllTrafficAreas")).Click();
            }

            Thread.Sleep(1000);

            if (!driver.FindElement(By.Id("chkSelectAllBuisinessType")).Selected)
            {
                driver.FindElement(By.Id("chkSelectAllBuisinessType")).Click();
            }
            else
            {
                driver.FindElement(By.Id("chkSelectAllBuisinessType")).Click();

                driver.FindElement(By.Id("chkSelectAllBuisinessType")).Click();
            }
            Thread.Sleep(1000);

            IWebElement trafficStartDate = driver.FindElement(By.Id("dteStartDate_dateInput"));
            trafficStartDate.Clear();
            trafficStartDate.SendKeys(CommonLogic.collectInvoice);

            Thread.Sleep(2000);
            IWebElement trafficEndDate = driver.FindElement(By.Id("dteEndDate_dateInput"));
            trafficEndDate.Clear();
            trafficEndDate.SendKeys(CommonLogic.deliverInvoice);

            Thread.Sleep(2000);

            driver.FindElement(By.Id("btnFilter")).Click();

            CommonLogic.resourceUnitsWindow = "";

            CommonLogic.resourceUnitsWindow = driver.WindowHandles.Last();

            driver.SwitchTo().Window(CommonLogic.mainWindow);

            Thread.Sleep(2000);

            driver.FindElement(By.LinkText("Planning")).Click();

            Thread.Sleep(1000);

            driver.FindElement(By.LinkText("Leg Planning")).Click();

            Thread.Sleep(5000);

            CommonLogic.legPlanningWindow = driver.WindowHandles.Last();

            driver.SwitchTo().Window(CommonLogic.legPlanningWindow);

            driver.SwitchTo().Window(CommonLogic.mainWindow);

            driver.Navigate().Refresh();

            driver.SwitchTo().Frame(1);

            Thread.Sleep(1000);
           
            IWebElement sourceElement = driver.FindElement(By.XPath("//*[text()[contains(.,'" + runIdInvoice + "')]]"));

            Actions builder = new Actions(driver);
            builder.ClickAndHold(sourceElement);
            Actions action = (Actions) builder.Build();
            action.Perform();

            Thread.Sleep(2000);

            driver.SwitchTo().Window(CommonLogic.legPlanningWindow);

            Thread.Sleep(2000);

            //IWebElement targetElement = driver.FindElement(By.XPath("(//td[@class='dhx_matrix_cell '])[1]"));

            //builder.MoveToElement(targetElement);
            //builder.Release(targetElement);
            //action = (Actions) builder.Build();
            //action.Perform();
            driver.Close();

        }

        private void changeBookedandPlannedTimes(string url)
        {
            var dateRaw = DateTime.Now;
            var todaysdate = DateTime.Now.ToString("dd/MM/yyyy");
            var tomorrowsDateRaw = dateRaw.AddDays(1);
            var tomorrowsdate = tomorrowsDateRaw.ToString("dd/MM/yyyy");
            var twoDaysAheadRaw = dateRaw.AddDays(2);
            var twoDaysAhead = twoDaysAheadRaw.ToString("dd/MM/yyyy");
            var threeDaysAheadRaw = dateRaw.AddDays(3);
            var threeDaysAhead = threeDaysAheadRaw.ToString("dd/MM/yyyy");

            IWebElement ordersNavigation = driver.FindElement(By.XPath("//*[@class='rmRootGroup rmHorizontal']//*[text()='Orders']"));
            ordersNavigation.Click();

            Thread.Sleep(5000);

            IWebElement addOrder = driver.FindElement(By.XPath("//*[@class='rmItem rmFirst']//*[text()='Add Order']"));
            addOrder.Click();

            driver.SwitchTo().Window(driver.WindowHandles.Last());

            // Add order screen

            Thread.Sleep(10000);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_cboClient_Input")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_cboClient_Input")).SendKeys("Proteo Test Client");
            Thread.Sleep(3000);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_cboClient_Input")).SendKeys(Keys.Enter);

            Thread.Sleep(5000);

            //IWebElement selectClient = driver.FindElement(By.XPath("//*[@class='rcbList']//*[text()='Proteo Test Client']"));
            //selectClient.Click();

            //// These three variables are for referring back to these elements on the invoicing stages

            //clientNameInvoice = selectClient.Text;

            //driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_dteCollectionFromDate_dateInput")).Click();
            //collectInvoice = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_dteCollectionFromDate_dateInput")).GetAttribute("value");

            //driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_dteDeliveryByDate_dateInput")).Click();
            //deliverInvoice = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_dteDeliveryByDate_dateInput")).GetAttribute("value");

            new SelectElement(driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_cboBusinessType"))).SelectByText("Full-Load");

            Thread.Sleep(1000);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_chkCreateJob")).Click();

            Thread.Sleep(5000);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_ucCollectionPoint_cboPoint_Input")).SendKeys("Proteo Test client");
            Thread.Sleep(3000);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_ucCollectionPoint_cboPoint_Input")).SendKeys(Keys.Down);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_ucCollectionPoint_cboPoint_Input")).SendKeys(Keys.Enter);

            Thread.Sleep(3000);

            IWebElement enterDeliverTo = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_ucDeliveryPoint_cboPoint_Input"));
            enterDeliverTo.SendKeys("24-7");

            Thread.Sleep(3000);

            IWebElement selectDeliverTo = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_ucDeliveryPoint_cboPoint_Input"));
            selectDeliverTo.SendKeys(Keys.Down);
            selectDeliverTo.SendKeys(Keys.Enter);

            Thread.Sleep(5000);

            IWebElement enterNoOfPallets = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_rntxtPallets"));
            enterNoOfPallets.Clear();
            enterNoOfPallets.SendKeys("8");

            Thread.Sleep(1000);

            IWebElement enterNoOfSpaces = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_rntxtPalletSpaces"));
            enterNoOfSpaces.Clear();
            enterNoOfSpaces.SendKeys("7.00");

            IWebElement enterWeight = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_rntxtWeight"));
            enterWeight.Clear();
            enterWeight.SendKeys("6");

            IWebElement enterCases = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_rntxtCartons"));
            enterCases.Clear();
            enterCases.SendKeys("5");

            IWebElement enterRate = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_rntOrderRate"));
            enterRate.Clear();
            enterRate.SendKeys("4.00");

            Thread.Sleep(2000);

            IWebElement addOrderButton = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_btnSubmit"));
            addOrderButton.Click();

            Thread.Sleep(2000);

            // Run screen

            driver.SwitchTo().Window(driver.WindowHandles.Last());

            Thread.Sleep(3000);

            IWebElement resourceThis = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_grdTrafficSheet_ctl00__0"));

            Actions rightClick = new Actions(driver);
            rightClick.ContextClick(resourceThis).Perform();

            Thread.Sleep(1000);

            driver.FindElement(By.XPath("//span[contains(text(), 'Change Booked Times')]")).Click();

            Thread.Sleep(3000);

            driver.SwitchTo().Window(driver.WindowHandles.Last());

            // Booked Times pop up
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_repBookedDateTimes_ctl00_dteCollectionFromDate_dateInput")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_repBookedDateTimes_ctl00_dteCollectionFromDate_dateInput")).SendKeys(tomorrowsdate);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_repBookedDateTimes_ctl01_dteCollectionFromDate_dateInput")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_repBookedDateTimes_ctl01_dteCollectionFromDate_dateInput")).SendKeys(twoDaysAhead);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_btnConfirm")).Click();

            Thread.Sleep(3000);

            driver.SwitchTo().Window(driver.WindowHandles.Last());

            IWebElement resourceThis2 = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_grdTrafficSheet_ctl00__0"));

            Actions rightClick2 = new Actions(driver);
            rightClick2.ContextClick(resourceThis2).Perform();

            Thread.Sleep(1000);

            driver.FindElement(By.XPath("//span[contains(text(), 'Change Planned Times')]")).Click();

            Thread.Sleep(3000);

            driver.SwitchTo().Window(driver.WindowHandles.Last());

            // Planned Times pop up
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_repLegs_ctl00_dteSDate_dateInput")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_repLegs_ctl00_dteSDate_dateInput")).SendKeys(twoDaysAhead);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_repLegs_ctl00_dteEDate_dateInput")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_repLegs_ctl00_dteEDate_dateInput")).SendKeys(threeDaysAhead);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_btnConfirm")).Click();

            clausedCallIn(url);
        }

        private void clausedCallIn(string url)
        {
            IWebElement ordersNavigation = driver.FindElement(By.XPath("//*[@class='rmRootGroup rmHorizontal']//*[text()='Orders']"));
            ordersNavigation.Click();

            Thread.Sleep(5000);

            IWebElement addOrder = driver.FindElement(By.XPath("//*[@class='rmItem rmFirst']//*[text()='Add Order']"));
            addOrder.Click();

            driver.SwitchTo().Window(driver.WindowHandles.Last());

            // Add order screen

            Thread.Sleep(10000);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_cboClient_Input")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_cboClient_Input")).SendKeys(CommonLogic.clientName);
            Thread.Sleep(3000);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_cboClient_Input")).SendKeys(Keys.Enter);

            Thread.Sleep(5000);

            //IWebElement selectClient = driver.FindElement(By.XPath("//*[@class='rcbList']//*[text()='Proteo Test Client']"));
            //selectClient.Click();

            //// These three variables are for referring back to these elements on the invoicing stages

            //clientNameInvoice = selectClient.Text;

            //driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_dteCollectionFromDate_dateInput")).Click();
            //collectInvoice = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_dteCollectionFromDate_dateInput")).GetAttribute("value");

            //driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_dteDeliveryByDate_dateInput")).Click();
            //deliverInvoice = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_dteDeliveryByDate_dateInput")).GetAttribute("value");

            new SelectElement(driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_cboBusinessType"))).SelectByText("Full-Load");

            Thread.Sleep(1000);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_chkCreateJob")).Click();

            Thread.Sleep(5000);

            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_ucCollectionPoint_cboPoint_Input")).SendKeys(CommonLogic.address);
            Thread.Sleep(3000);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_ucCollectionPoint_cboPoint_Input")).SendKeys(Keys.Down);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_ucCollectionPoint_cboPoint_Input")).SendKeys(Keys.Enter);

            Thread.Sleep(3000);

            IWebElement enterDeliverTo = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_ucDeliveryPoint_cboPoint_Input"));
            enterDeliverTo.SendKeys("24-7");

            Thread.Sleep(3000);

            IWebElement selectDeliverTo = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_ucDeliveryPoint_cboPoint_Input"));
            selectDeliverTo.SendKeys(Keys.Down);
            selectDeliverTo.SendKeys(Keys.Enter);

            Thread.Sleep(5000);

            IWebElement enterNoOfPallets = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_rntxtPallets"));
            enterNoOfPallets.Clear();
            enterNoOfPallets.SendKeys("8");

            Thread.Sleep(1000);

            IWebElement enterNoOfSpaces = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_rntxtPalletSpaces"));
            enterNoOfSpaces.Clear();
            enterNoOfSpaces.SendKeys("7.00");

            IWebElement enterWeight = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_rntxtWeight"));
            enterWeight.Clear();
            enterWeight.SendKeys("6");

            IWebElement enterCases = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_rntxtCartons"));
            enterCases.Clear();
            enterCases.SendKeys("5");

            IWebElement enterRate = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_rntOrderRate"));
            enterRate.Clear();
            enterRate.SendKeys("4.00");

            Thread.Sleep(2000);

            IWebElement addOrderButton = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucOrder_btnSubmit"));
            addOrderButton.Click();

            Thread.Sleep(2000);

            // Run screen

            driver.SwitchTo().Window(driver.WindowHandles.Last());

            Thread.Sleep(3000);

            IWebElement resourceThis = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_grdTrafficSheet_ctl00__0"));

            Actions rightClick = new Actions(driver);
            rightClick.ContextClick(resourceThis).Perform();

            Thread.Sleep(4000);

            driver.FindElement(By.XPath("//span[contains(text(), 'Resource This')]")).Click();

            // Resource This Screen

            driver.SwitchTo().Window(driver.WindowHandles.Last());

            Thread.Sleep(4000);

            IWebElement enterDriver = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboDriver_Input"));
            enterDriver.SendKeys(CommonLogic.displayName);

            Thread.Sleep(1000);

            enterDriver.SendKeys(Keys.Down);

            Thread.Sleep(2000);

            IWebElement selectDriver = driver.FindElement(By.ClassName("rcbHovered"));
            selectDriver.Click();

            Thread.Sleep(5000);

            //var vehicleNameEntered = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboVehicle_Input"));
            //vehicleNameEntered.GetAttribute("id");
            //Assert.AreEqual(CommonLogic.vehicleName, vehicleNameEntered);

            IWebElement enterTrailer = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboTrailer_Input"));
            enterTrailer.SendKeys(CommonLogic.displayName);

            Thread.Sleep(5000);

            IWebElement selectTrailer = driver.FindElement(By.ClassName("rcbHovered"));
            selectTrailer.Click();

            Thread.Sleep(5000);

            IWebElement updateResourceButton = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_btnUpdateAndConfirm"));
            updateResourceButton.Click();

            // Quick Communicate This

            Thread.Sleep(5000);

            driver.SwitchTo().Window(driver.WindowHandles.Last());

            Thread.Sleep(5000);

            IWebElement quickCommunicateThis = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_grdTrafficSheet_ctl00__0"));

            Actions rightClick2 = new Actions(driver);
            rightClick2.ContextClick(quickCommunicateThis).Perform();

            Thread.Sleep(5000);

            driver.FindElement(By.XPath("//span[contains(text(), 'Quick Communicate This')]")).Click();

            Thread.Sleep(10000);

            // Call in 

            IWebElement callInLoad = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ctl20_repOH_ctl01_btnRecordCallIn"));
            callInLoad.Click();

            Thread.Sleep(4000);

            driver.FindElement(By.Name("ctl00$ContentPlaceHolder1$btnRedeliver")).Click();
            //driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_btnRedeliver")).Click();
            Thread.Sleep(2000);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_btnPartial")).Click();
            Thread.Sleep(2000);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtAttemptedCollectionReference")).Click();
            Thread.Sleep(2000);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtAttemptedCollectionReference")).Clear();
            Thread.Sleep(2000);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtAttemptedCollectionReference")).SendKeys("test");
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtAttemptedClientContact")).Click();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtAttemptedClientContact")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtAttemptedClientContact")).SendKeys("test");
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_dteCollectionFromDate")).Click();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_dteCollectionFromDate")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_dteCollectionFromDate")).SendKeys(CommonLogic.todaysDate);
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Points'])[1]/following::label[1]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Points'])[1]/following::label[1]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Yes'])[1]/following::b[1]")).Click();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_chkChangeDeliveryDate")).Click();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_chkChangeDeliveryDate")).Click();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_chkCollectGoodsElsewhere")).Click();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucNewCollectionPoint_cboPoint_Input")).Click();
            Thread.Sleep(2000);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucNewCollectionPoint_cboPoint_Input")).SendKeys(Keys.Down);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_ucNewCollectionPoint_cboPoint_Input")).SendKeys(Keys.Enter);
            Thread.Sleep(2000);
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Are you charging for this?'])[1]/following::td[1]")).Click();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_chkCharging")).Click();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtExtraAmount")).Click();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtExtraAmount")).Clear();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtExtraAmount")).SendKeys("500");
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Type of Extra'])[1]/following::td[1]")).Click();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboExtraType")).Click();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboExtraType")).Click();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboExtraState")).Click();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_cboExtraState")).Click();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtExtraCustomReason")).Click();
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_btnSave")).Click();

            Thread.Sleep(5000);
            CommonLogic.deleteClient(url,driver);
            Thread.Sleep(2000);
            deleteVehicleDriverTrailer(url);
    }

    private void deleteVehicleDriverTrailer(string url)
    {
        CommonLogic.deleteDriver(CommonLogic.displayName, driver);

        Thread.Sleep(5000);

        CommonLogic.deleteVehicle(CommonLogic.vehicleName, driver);

        Thread.Sleep(5000);

        CommonLogic.deleteTrailer(CommonLogic.trailerRef, driver);

        addExtra(url, driver);
    }
  }
}